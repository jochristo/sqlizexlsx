﻿using SQlizeXlsx.Core.Generators;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using static SQlizeXlsx.Core.Attributes;
using static SQlizeXlsx.Core.Generators.RandomDateTime;

namespace SQlizeXlsx.Core.Randomized
{
    /// <summary>
    /// Defines methods, classes, and tools to generate random sequences for fields and properties of objects.
    /// </summary>
    public static class Randomizer
    {
        /// <summary>
        /// Contains static data to choose from used in generator instances.
        /// </summary>
        public class Library
        {
            /// <summary>
            /// Contains all available language culture names.
            /// </summary>
            private string[] _cultures =  { "af-ZA","sq-AL","ar-DZ","ar-BH","ar-EG","ar-IQ","ar-JO","ar-KW","ar-LB","ar-LY","ar-MA","ar-OM","ar-QA","ar-SA",
                "ar-SY","ar-TN","ar-AE","ar-YE","hy-AM","Cy-az-AZ","Lt-az-AZ","eu-ES","be-BY","bg-BG","ca-ES","zh-CN","zh-HK","zh-MO","zh-SG","zh-TW","zh-CHS",
                "zh-CHT","hr-HR","cs-CZ","da-DK","div-MV","nl-BE","nl-NL","en-AU","en-BZ","en-CA","en-CB","en-IE","en-JM","en-NZ","en-PH","en-ZA","en-TT",
                "en-GB","en-US","en-ZW","et-EE","fo-FO","fa-IR","fi-FI","fr-BE","fr-CA","fr-FR","fr-LU","fr-MC","fr-CH","gl-ES","ka-GE","de-AT","de-DE","de-LI",
                "de-LU","de-CH","el-GR","gu-IN","he-IL","hi-IN","hu-HU","is-IS","id-ID","it-IT","it-CH","ja-JP","kn-IN","kk-KZ","kok-IN","ko-KR","ky-KZ",
                "lv-LV","lt-LT","mk-MK","ms-BN","ms-MY","mr-IN","mn-MN","nb-NO","nn-NO","pl-PL","pt-BR","pt-PT","pa-IN","ro-RO","ru-RU","sa-IN","Cy-sr-SP",
                "Lt-sr-SP","sk-SK","sl-SI","es-AR","es-BO","es-CL","es-CO","es-CR","es-DO","es-EC","es-SV","es-GT","es-HN","es-MX","es-NI","es-PA","es-PY",
                "es-PE","es-PR","es-ES","es-UY","es-VE","sw-KE","sv-FI","sv-SE","syr-SY","ta-IN","tt-RU","te-IN","th-TH","tr-TR","uk-UA","ur-PK","Cy-uz-UZ",
                "Lt-uz-UZ","vi-VN"};

            /// <summary>
            /// Contains gender values.
            /// </summary>
            private string[] _genders = { "Female", "Male", "Other" };

            /// <summary>
            /// Contains some common language culture names.
            /// </summary>
            private string[] _commonCultures = { "zh-CN", "nl-NL", "en-US", "en-GB", "en-ZA", "en-AU", "fr-FR", "de-DE", "it-IT", "ja-JP", "ru-RU", "es-ES", "el-GR", "ar-AE"};
            
            public string[] Cultures
            {
                get
                {
                    return _cultures;
                }

                set
                {
                    _cultures = value;
                }
            }

            public string[] Genders
            {
                get
                {
                    return _genders;
                }

                set
                {
                    _genders = value;
                }
            }

            public string[] CommonCultures
            {
                get
                {
                    return _commonCultures;
                }

                set
                {
                    _commonCultures = value;
                }
            }
        }
        
        /// <summary>
        /// Returns an enumeration of T objects with randomized string-based property values in the source collection based on <see cref="SQlizeXlsx.Core.Attributes.RandomValue"/> attribute applied to the fields and the properties of each object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IEnumerable<T> Randomize<T>(IEnumerable<T> source) where T  : new()                                                                                                      
        {
            T t = new T();            
            var allCharsGenerator = new CharsGenerator<AllCharsGenerator>();
            var emailGenerator = new CharsGenerator<EmailCharsGenerator>();
            var emailDomainExtentionGenerator = new CharsGenerator<EmailDomainExtentionGenerator>();
            var digitGenerator = new CharsGenerator<DigitGenerator>();
            var numGenerator = new NumberGenerator();
            Library lib = new Library();
            var emails = new HashSet<string>();
            var nicknames = new HashSet<string>();
            var datesSet = new HashSet<string>();
            var dobs = new HashSet<string>();
            var randomDateTime = new RandomDateTime();
            var fixedCreatedDate = new RandomDateTime().NextToString(DatetimeType.DateTime);
            var fields = t.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            var list = ((IEnumerable<T>)source).ToList();
            
            list.AsParallel().WithDegreeOfParallelism(4).ToList().ForEach(item =>
            {              
                fields.AsParallel().WithDegreeOfParallelism(4).ToList().ToList().ForEach(f =>
                {
                    var attributes = f.GetCustomAttributes();
                    attributes.AsParallel().WithDegreeOfParallelism(4).ToList().ToList().ForEach(a =>
                    {
                        if (a is RandomValue)
                        {
                            RandomValue rvt = a as RandomValue;
                            var columnAttributeType = rvt.ElementAttributeType;
                            switch (columnAttributeType)
                            {
                                case ElementAttributeType.DateTime:
                                    var d = randomDateTime.NextToString(DatetimeType.DateTime);
                                    
                                    while (datesSet.Contains(d))
                                    {
                                        d = randomDateTime.NextToString(DatetimeType.DateTime);
                                    }
                                    datesSet.Add(d);
                                    
                                    f.SetValue(item, d);
                                    break;
                                case ElementAttributeType.FixedDateTime:                                    
                                    f.SetValue(item, fixedCreatedDate);
                                    break;
                                case ElementAttributeType.Date:
                                    var dob = randomDateTime.NextToString(DatetimeType.Date);
                                    while (dobs.Contains(dob))
                                    {
                                        dob = randomDateTime.NextToString(DatetimeType.Date);
                                    }
                                    dobs.Add(dob);
                                    f.SetValue(item, dob);
                                    break;
                                case ElementAttributeType.Nickname:
                                    var nick = allCharsGenerator.GetRandomChars(numGenerator.NextInteger(4, 16));
                                    while (nicknames.Contains(nick))
                                    {
                                        nick = allCharsGenerator.GetRandomChars(numGenerator.NextInteger(4, 16));
                                    }
                                    nicknames.Add(nick);
                                    f.SetValue(item, nick);
                                    break;
                                case ElementAttributeType.Email:
                                    var email = emailGenerator.GetRandomChars(numGenerator.NextInteger(3, 9)) + "@" + emailGenerator.GetRandomChars(numGenerator.NextInteger(5,11)).ToLower() + "." + emailDomainExtentionGenerator.GetRandomChars(numGenerator.NextInteger(2, 4));
                                    while (emails.Contains(email))
                                    {
                                        email = emailGenerator.GetRandomChars(numGenerator.NextInteger(3, 9)) + "@" + emailGenerator.GetRandomChars(numGenerator.NextInteger(5, 11)).ToLower() + "." + emailDomainExtentionGenerator.GetRandomChars(numGenerator.NextInteger(2, 4));
                                    }
                                    emails.Add(email);
                                    f.SetValue(item, email);
                                    break;
                                case ElementAttributeType.Phone:
                                    f.SetValue(item, "+" + digitGenerator.GetRandomChars(12));                                    
                                    break;
                                case ElementAttributeType.Gender:
                                    f.SetValue(item, lib.Genders[numGenerator.NextInteger(0, lib.Genders.Count())]);
                                    break;
                                case ElementAttributeType.Culture:
                                    f.SetValue(item, lib.CommonCultures[numGenerator.NextInteger(0, lib.CommonCultures.Count())]);
                                    break;
                                case ElementAttributeType.Long:
                                    f.SetValue(item, numGenerator.NextInteger(1, 1001).ToString());
                                    break;
                                case ElementAttributeType.Level:
                                    f.SetValue(item, numGenerator.NextInteger(1, 1).ToString());
                                    break;
                                case ElementAttributeType.Likes:
                                    f.SetValue(item, numGenerator.NextInteger(1, 201).ToString());
                                    break;
                                case ElementAttributeType.Longitude:
                                    f.SetValue(item, numGenerator.NextDouble(-179,179, numGenerator.NextInteger(1,7)));
                                    break;
                                case ElementAttributeType.Latitude:
                                    f.SetValue(item, numGenerator.NextDouble(-89, 89, numGenerator.NextInteger(1, 7)));
                                    break;
                            }
                        }
                    });
                });
            });
            return (IEnumerable<T>)list;
        }

        /// <summary>
        /// Returns a randomized T instance from given object based on the attribute type annotated to its field members.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static T Randomize<T> (T instance) where T : new()
        {
            var allCharsGenerator = new CharsGenerator<AllCharsGenerator>();
            var emailGenerator = new CharsGenerator<EmailCharsGenerator>();
            var digitGenerator = new CharsGenerator<DigitGenerator>();
            var numGenerator = new NumberGenerator();
            Library lib = new Library();
            var emails = new HashSet<string>();
            var nicknames = new HashSet<string>();
            var datesSet = new HashSet<string>();
            var dobs = new HashSet<string>();
            var randomDateTime = new RandomDateTime();
            T copy = instance;
            var fields = instance.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            fields.ToList().ForEach(f =>
            {
                var attributes = f.GetCustomAttributes();
                attributes.ToList().ForEach(a =>
                {
                    if (a is RandomValue)
                    {
                        RandomValue rvt = a as RandomValue;
                        var columnAttributeType = rvt.ElementAttributeType;
                        switch (columnAttributeType)
                        {
                            case ElementAttributeType.DateTime:
                                var d = randomDateTime.NextToString(DatetimeType.DateTime);
                                while (datesSet.Contains(d))
                                {
                                    d = randomDateTime.NextToString(DatetimeType.DateTime);
                                }
                                datesSet.Add(d);
                                f.SetValue(copy, d);
                                break;

                            case ElementAttributeType.Date:
                                var dob = randomDateTime.NextToString(DatetimeType.Date);
                                while (dobs.Contains(dob))
                                {
                                    dob = randomDateTime.NextToString(DatetimeType.Date);
                                }
                                dobs.Add(dob);
                                f.SetValue(copy, dob);
                                break;

                            case ElementAttributeType.Nickname:
                                var nick = allCharsGenerator.GetRandomChars(15);
                                while (nicknames.Contains(nick))
                                {
                                    nick = allCharsGenerator.GetRandomChars(15);
                                }
                                nicknames.Add(nick);
                                f.SetValue(copy, nick);
                                break;

                            case ElementAttributeType.Email:
                                var email = emailGenerator.GetRandomChars(8) + "@" + emailGenerator.GetRandomChars(4) + "." + emailGenerator.GetRandomChars(3);
                                while (emails.Contains(email))
                                {
                                    email = emailGenerator.GetRandomChars(8) + "@" + emailGenerator.GetRandomChars(4) + "." + emailGenerator.GetRandomChars(3);
                                }
                                emails.Add(email);
                                f.SetValue(copy, email);
                                break;

                            case ElementAttributeType.Phone:
                                f.SetValue(copy, "+" + digitGenerator.GetRandomChars(12));
                                break;

                            case ElementAttributeType.Gender:
                                f.SetValue(copy, lib.Genders[numGenerator.NextInteger(0, lib.Genders.Count())]);
                                break;

                            case ElementAttributeType.Culture:
                                f.SetValue(copy, lib.CommonCultures[numGenerator.NextInteger(0, lib.CommonCultures.Count())]);
                                break;

                            case ElementAttributeType.Long:
                                f.SetValue(copy, numGenerator.NextInteger(1, 1000).ToString());
                                break;
                            case ElementAttributeType.Level:
                                f.SetValue(copy, numGenerator.NextInteger(1, 10).ToString());
                                break;

                        }
                    }
                });

            });
            return copy;
        }

    }
}

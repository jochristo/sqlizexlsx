﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Randomized
{
    /// <summary>
    /// Provides hashing utilities using SHA1 and PBKDF2 algorithms.
    /// </summary>
    public static class Hashing
    {
        public static string Sha1Hash(string value)
        {
            const int iterations = 1000;

            // derive secret key                
            byte[] saltToBytes = Encoding.UTF8.GetBytes(CreateSalt(value));
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(value, 16, iterations);

            // produce a sequence string containing  some prefix and secret key for given value
            return iterations + ":" + pbkdf2.Salt.ToHexSigned() + ":" + pbkdf2.GetBytes(32).ToHexSigned();
        }

        public static byte[] GetSalt()
        {
            var rng = RandomNumberGenerator.Create();
            var salt = new byte[16];
            rng.GetBytes(salt);
            return salt;
        }

        public static byte[] ComputeHash(byte[] salt, string password)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(password);
            byte[] saltBytes = salt;
            byte[] concat = new byte[plainTextBytes.Length + saltBytes.Length];
            System.Buffer.BlockCopy(plainTextBytes, 0, concat, 0, plainTextBytes.Length);
            System.Buffer.BlockCopy(saltBytes, 0, concat, plainTextBytes.Length, saltBytes.Length);

            SHA256Managed hash = new SHA256Managed();

            byte[] tHashBytes = hash.ComputeHash(concat);

            return tHashBytes;
        }

        private static string CreateSalt(string username)
        {
            byte[] bytes;
            string salt;
            bytes = Encoding.UTF8.GetBytes(username);
            long XORED = 0x00;

            foreach (int x in bytes)
                XORED = XORED ^ x;

            Random rand = new Random(Convert.ToInt32(XORED));
            salt = rand.Next().ToString();
            salt += rand.Next().ToString();
            salt += rand.Next().ToString();
            salt += rand.Next().ToString();
            return salt;
        }

        /// <summary>
        /// Converts this instance of <see cref="System.Byte"/> array to hexadecimal format.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string ToHex(this byte[] array)
        {
            // Create a new Stringbuilder to collect the bytes and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string.
            for (int i = 0; i < array.Length; i++)
            {
                sBuilder.Append(array[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// Converts each <see cref="System.Byte"/> element to signed format and this instance of <see cref="System.Byte"/> array to hexadecimal format.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string ToHexSigned(this byte[] array)
        {
            // Create a new Stringbuilder to collect the bytes and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string.
            for (int i = 0; i < array.Length; i++)
            {
                //-128 to 127
                string item = array[i].ToString();
                int assigned = array[i];
                if (assigned < -128 && assigned > 127)
                {
                    assigned = assigned - 256;
                }
                sBuilder.Append(assigned.ToString("x2"));
            }
            return sBuilder.ToString();
        }

    }
}

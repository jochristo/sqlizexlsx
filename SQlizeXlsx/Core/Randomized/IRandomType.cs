﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Randomized
{
    /// <summary>
    /// Defines method to acquire the proper alphabet for a random-value generator to use.
    /// </summary>
    public interface IRandomType
    {
        char[] alphabet();
    }
}

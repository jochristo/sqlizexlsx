﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Exceptions
{
    internal static class AppExceptions
    {

        #region Exception Types

        public class EmptyFileException : Exception
        {
            public EmptyFileException()
            : base() { }

            public EmptyFileException(string message)
            : base(message) { }
        }

        public class WrongFileFormatException : Exception
        {
            public WrongFileFormatException()
            : base() { }

            public WrongFileFormatException(string message)
            : base(message) { }
        }

        public class InvalidFormatException : Exception
        {
            public InvalidFormatException()
            : base() { }

            public InvalidFormatException(string message)
            : base(message) { }
        }

        public class InvalidJsonFormatException : Exception
        {
            public InvalidJsonFormatException()
            : base() { }

            public InvalidJsonFormatException(string message)
            : base(message) { }
        }

        public class EmptyCellException : Exception
        {
            public EmptyCellException()
            : base() { }

            public EmptyCellException(string message)
            : base(message) { }
        }

        public class DuplicateDataException : Exception
        {
            public DuplicateDataException()
            : base() { }

            public DuplicateDataException(string message)
            : base(message) { }
        }

        #endregion
    }
}

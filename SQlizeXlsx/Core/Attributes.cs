﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Contains definitions for custom application attributes.
    /// </summary>
    public class Attributes
    {
        /// <summary>
        /// Specifies the element's value type to be used when generating random sequences of characters.
        /// </summary>
        [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct | AttributeTargets.Property | AttributeTargets.Field)]
        public class RandomValue : System.Attribute
        {
            private ElementAttributeType _elementAttributeType;
            public ElementAttributeType ElementAttributeType
            {
                get
                {
                    return _elementAttributeType;
                }

                set
                {
                    _elementAttributeType = value;
                }
            }

            public RandomValue(ElementAttributeType elementAttributeType)
            {
                this.ElementAttributeType = elementAttributeType;
            }
        }

        /// <summary>
        /// Specifies the attribute type on which the <see cref="SQlizeXlsx.Core.Attributes.RandomValue"/> attribute is applied.
        /// </summary>
        public enum ElementAttributeType
        {
            Email,
            DateTime,
            FixedDateTime,
            Date,
            Gender,
            Culture,
            Phone,
            Nickname,
            Password,
            Longitude,
            Latitude,
            Long,
            Level,
            Likes,
            ForeignKey
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Collections
{
    /// <summary>
    /// Represents a unique Phrase-Profile key/value pair to compare against.
    /// </summary>
    internal class PhraseProfilePair
    {
        private string phraseid;
        private string profileid;

        /// <summary>
        /// Gets or sets the phrase id.
        /// </summary>
        public string Phraseid
        {
            get
            {
                return phraseid;
            }

            set
            {
                phraseid = value;
            }
        }

        /// <summary>
        /// Gets or sets the profile id.
        /// </summary>
        public string Profileid
        {
            get
            {
                return profileid;
            }

            set
            {
                profileid = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PhraseProfilePair"/>  class with default constructor.
        /// </summary>
        public PhraseProfilePair() { }

        /// <summary>
        /// Initializes a new instance of <see cref="PhraseProfilePair"/>  class with given phrase id and profile id.
        /// </summary>
        /// <param name="phraseid">The id of phrase.</param>
        /// <param name="profileid">The id of profile.</param>
        public PhraseProfilePair(string phraseid, string profileid)
        {
            this.phraseid = phraseid; this.profileid = profileid;
        }

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // Try to cast to PhraseProfilePair.
            PhraseProfilePair p = obj as PhraseProfilePair;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (phraseid == p.phraseid) && (profileid == p.profileid);
        }

        public override int GetHashCode()
        {
            // Some hash code
            return Int32.Parse(this.phraseid) ^ Int32.Parse(this.profileid);
        }
    }
}

﻿using System.Threading.Tasks;
using SQlizeXlsx.Linq2Excel;
using SQlizeXlsx.Core.Domain;

namespace SQlizeXlsx.Core.Collections
{
    /// <summary>
    /// Loads and holds a collection of <see cref="AnswerDTO"/> objects loaded from given Excel file.
    /// </summary>
    public class AnswerCollection : DataCollection<AnswerDTO>
    {
        private string _filename = string.Empty;
        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }

        /// <summary>
        /// Initializes the collection of <see cref="AnswerDTO"/> objects with given filename path.
        /// </summary>
        /// <param name="size"></param>
        public AnswerCollection(string filename) : base()
        {
            this.Filename = filename;
        }

        /// <summary>
        /// Returns whether the collction is random or file generated.
        /// </summary>
        /// <returns></returns>
        public override bool IsRandom()
        {
            return false;
        }

        /// <summary>
        /// Returns a file-parsed collection of <see cref="AnswerDTO"/> objects as a <see cref="ReferenceStorage{string, AnswerDTO}"/> object.
        /// The Value property of <see cref="ReferenceStorage{string, AnswerDTO}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, AnswerDTO}"/> instance holds an enumeration of <see cref="AnswerDTO"/> objects.
        /// </summary>
        /// <returns></returns>
        public async override Task<ReferenceStorage<string, AnswerDTO>> GetDataAsync()
        {
            var data = await SQLize.ScriptWithResultAsync<AnswerDTO>(this.Filename);
            this.Source = data.Data;
            return data;
        }
    }
}

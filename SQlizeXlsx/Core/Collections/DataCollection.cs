﻿using SQlizeXlsx.Linq2Excel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Base class to support data collections for random or file generated objects.
    /// </summary>
    /// <typeparam name="T">The type of objects contained in the collection.</typeparam>
    public abstract class DataCollection<T>
    {        
        private IEnumerable<T> _source = new List<T>();
        private Task<ReferenceStorage<string, T>> _data;

        /// <summary>
        /// Gets or sets the source enumeration to use for inherited collections.
        /// </summary>
        public IEnumerable<T> Source
        {
            get
            {
                return _source;
            }

            set
            {
                _source = value;
            }
        }

        /// <summary>
        /// Holds and returns the data as an asynchronous operation.
        /// The Value property of <see cref="ReferenceStorage{string, T}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, T}"/> instance holds an enumeration of T objects.
        /// </summary>
        /// <returns>The  <see cref=".ReferenceKeeper{string, T}"/> instance.</returns>
        public virtual Task<ReferenceStorage<string, T>> GetDataAsync() { return _data; }

        /// <summary>
        /// Determines whether the collection is randomly generated or loaded from a data file.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsRandom() { return false; }
    }
}

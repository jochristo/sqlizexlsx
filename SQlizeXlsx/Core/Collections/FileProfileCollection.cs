﻿using System.Threading.Tasks;
using SQlizeXlsx.Linq2Excel;
using SQlizeXlsx.Core.Domain;

namespace SQlizeXlsx.Core.Collections
{
    public class FileProfileCollection : ProfileCollection
    {
        private string _filename = string.Empty;
        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }
        public FileProfileCollection()
        {

        }

        public FileProfileCollection(string filename)
        {
            this._filename = filename;
        }

        public override async Task<ReferenceStorage<string, ProfileDTO>> GetDataAsync()
        {
            ProfileDTO profileDTO = new FileBasedProfileDTO();
            ReferenceStorage<string, ProfileDTO> referenceKeeper = new ReferenceStorage<string, ProfileDTO>();
            var iMappable = (IMappable)profileDTO;
            var data = await SQLize.ScriptWithResultAsync<ProfileDTO>(this.Filename);
            this.Source = data.Data;
            return data;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using SQlizeXlsx.Linq2Excel;
using System.Collections.Generic;
using static SQlizeXlsx.Linq2Excel.Validation;
using static SQlizeXlsx.Linq2Excel.SQLize;
using SQlizeXlsx.Core.Domain;
using SQlizeXlsx.Core.Randomized;
using SQlizeXlsx.Core.Generators;

namespace SQlizeXlsx.Core.Collections
{
    /// <summary>
    /// Loads and holds a collection of <see cref="PhraseCategoryDTO"/> objects loaded from given Excel file or randomly generated.
    /// </summary>
    public class PhraseCategoryCollection : DataCollection<PhraseCategoryDTO>
    {
        private string _filename = string.Empty;
        private bool _isGenerated = false;
        private IEnumerable<CategoryDTO> _categories;
        private IEnumerable<PhraseDTO> _phrases;

        /// <summary>
        /// Gets or sets the filename (path to file) to generate the collection from.
        /// </summary>
        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }

        /// <summary>
        /// Gets or sets the source collection of <see cref="CategoryDTO"/> objects.
        /// </summary>
        public IEnumerable<CategoryDTO> Categories
        {
            get
            {
                return _categories;
            }

            set
            {
                _categories = value;
            }
        }

        /// <summary>
        /// Gets or sets the source collection of <see cref="PhraseDTO"/> objects.
        /// </summary>
        public IEnumerable<PhraseDTO> Phrases
        {
            get
            {
                return _phrases;
            }

            set
            {
                _phrases = value;
            }
        }

        /// <summary>
        /// Gets or sets the value to determine whether in random or file mode.
        /// </summary>
        public bool IsGenerated
        {
            get
            {
                return _isGenerated;
            }

            set
            {
                _isGenerated = value;
            }
        }

        /// <summary>
        /// Initializes the collection of <see cref="PhraseCategoryDTO"/> objects with given filename path.
        /// </summary>
        /// <param name="size"></param>
        public PhraseCategoryCollection(string filename) : base()
        {
            this.Filename = filename;
        }
        
        /// <summary>
        /// Initializes the collection of <see cref="PhraseCategoryDTO"/> objects with given collections and randomness indicator.
        /// If random mode is opted all data will be generated randomly based on the size and  data of phrase and category collections passed to the constructor.
        /// </summary>
        /// <param name="phrases">Phrase collection.</param>
        /// <param name="categories">Category collection.</param>
        /// <param name="isGenerated">Indicator of random mode.</param>
        public PhraseCategoryCollection(IEnumerable<PhraseDTO> phrases, IEnumerable<CategoryDTO> categories, bool isGenerated) : base()
        {
            this.Categories = categories;
            this.Phrases = phrases;
            this.IsGenerated = IsGenerated;
        }

        /// <summary>
        /// Initializes the collection of <see cref="PhraseCategoryDTO"/> objects with given collections.
        /// If random mode is opted all data will be generated randomly based on the size and  data of phrase and category collections passed to the constructor.
        /// </summary>
        /// <param name="phrases">Phrase collection.</param>
        /// <param name="categories">Category collection.</param>
        public PhraseCategoryCollection(IEnumerable<PhraseDTO> phrases, IEnumerable<CategoryDTO> categories)
        {
            this.Categories = categories;
            this.Phrases = phrases;
            this.IsGenerated = true;
        }

        /// <summary>
        /// Returns whether the collection is random or file-generated.
        /// </summary>
        /// <returns></returns>
        public override bool IsRandom()
        {
            return this.IsGenerated;
        }

        /// <summary>
        /// Returns a file-parsed collection of <see cref="PhraseCategoryDTO"/> objects as a <see cref="ReferenceStorage{string, PhraseCategoryDTO}"/> object.
        /// The Value property of <see cref="ReferenceStorage{string, PhraseCategoryDTO}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, PhraseCategoryDTO}"/> instance holds an enumeration of <see cref="PhraseCategoryDTO"/> objects.
        /// </summary>
        /// <returns></returns>
        public override async Task<ReferenceStorage<string, PhraseCategoryDTO>> GetDataAsync()
        {
            ReferenceStorage<string, PhraseCategoryDTO> data = null;
            if(IsRandom())
            {
                data = await this.GetRandomData();
            }
            else
            {
                data = await ScriptWithResultAsync<PhraseCategoryDTO>(this.Filename);
            }
            this.Source = data.Data;
            return data;
        }

        /// <summary>
        /// Returns a random collection of <see cref="PhraseCategoryDTO"/> objects as a <see cref="ReferenceStorage{T, D}"/> instance.
        /// </summary>
        /// <returns></returns>
        private async Task<ReferenceStorage<string, PhraseCategoryDTO>> GetRandomData()
        {
            return await Task<ReferenceStorage<string, PhraseCategoryDTO>>.Run( () =>
            {
                if (this.Categories == null)
                {
                    throw new ArgumentException("Categories collection is null");
                }
                if (this.Phrases == null)
                {
                    throw new ArgumentException("Phrases collection is null");
                }

                var generator = new NumberGenerator();
                var data = new ReferenceStorage<string, PhraseCategoryDTO>();
                var target = new List<PhraseCategoryDTO>();
                var phrases = (List<PhraseDTO>)Phrases;
                var categories = (List<CategoryDTO>)Categories;
                var categoriesCount = categories.Count;
                var phrasesCount = phrases.Count;

                // Start generating items randomly based on phrases and categories ids
                var i = 1;
                phrases.ForEach(p =>
                {
                    var item = new PhraseCategoryDTO();
                    item.FK_phrase_id = i.ToString();
                    item.FK_category_id = generator.NextInteger(1, categoriesCount+1).ToString();
                    target.Add(item);
                    i++;
                });

                // If collections are empty produce error message.
                var results = new List<Result>();
                if (phrases.Count == 0)
                {
                    var result = new Result(false, "Cannot generate phrase-category data because phrases collection is empty");
                    results.Add(result);
                }
                if (categories.Count == 0)
                {
                    var result = new Result(false, "Cannot generate phrase-category data because categories collection is empty");
                    results.Add(result);
                }

                var k = 1; // start at 1 because of excel header row!
                target.ForEach(d =>
                {
                    var isValid = d.IsValid(k);
                    if (!isValid)
                    {
                        var result = d.Validate(k);
                        results.Add(result);
                    }
                    k++;
                });

                // ERRORS FOUND
                if (results.Count > 0)
                {
                    IMappable iMappable = new PhraseCategoryDTO();
                    var message = "[" + iMappable.Name() + "]\n";
                    results.ForEach(m =>
                    {
                        message += m.Message + "\n";
                    });

                    data.Value = message;
                    data.IsValid = false;
                    return data;
                }
                // Store actual list data and SQL insert string to ref instance
                data.Value = JoinSqlInserts(target);
                data.IsValid = true;
                data.Data = target;
                this.Source = data.Data;
                return data;
            });
        }
    }
}


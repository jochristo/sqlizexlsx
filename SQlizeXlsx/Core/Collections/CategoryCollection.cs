﻿using System.Threading.Tasks;
using SQlizeXlsx.Linq2Excel;
using SQlizeXlsx.Core.Domain;

namespace SQlizeXlsx.Core.Collections
{
    /// <summary>
    /// Loads and holds a collection of <see cref="CategoryDTO"/> objects loaded from given Excel file.
    /// </summary>
    public class CategoryCollection : DataCollection<CategoryDTO>
    {
        private string _filename = string.Empty;
        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }

        /// <summary>
        /// Initializes the collection of <see cref="CategoryDTO"/> objects with given filename path.
        /// </summary>
        /// <param name="size"></param>
        public CategoryCollection(string filename) : base()
        {
            this.Filename = filename;
        }

        public override bool IsRandom()
        {
            return false;
        }

        /// <summary>
        /// Returns a file-parsed collection of <see cref=CategoryDTO"/> objects as a <see cref="ReferenceStorage{string, CategoryDTO}"/> object.
        /// The Value property of <see cref="ReferenceStorage{string, CategoryDTO}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, CategoryDTO}"/> instance holds an enumeration of <see cref="CategoryDTO"/> objects.
        /// </summary>
        /// <returns></returns>
        public override async Task<ReferenceStorage<string, CategoryDTO>> GetDataAsync()
        {
            var data = await SQLize.ScriptWithResultAsync<CategoryDTO>(this.Filename);
            this.Source = data.Data;
            return data;
        }
    }
}

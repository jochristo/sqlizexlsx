﻿using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Defines methods to acquire data randomly or from data file, for instance from an Excel spredsheet.
    /// </summary>
    /// <typeparam name="T">The type of the data collection</typeparam>
    public interface IDataCollection<T>
    {
        /// <summary>
        /// Holds and returns the data as an asynchronous operation.
        /// The Value property of <see cref="ReferenceStorage{string, T}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, T}"/> instance holds an enumeration of T objects.
        /// </summary>
        /// <returns>The  <see cref=".ReferenceStorage{string, T}"/> instance.</returns>
        Task<ReferenceStorage<string, T>> GetDataAsync();

        /// <summary>
        /// Defines if the data collection is randomly generated or loaded from data file.
        /// </summary>
        /// <returns></returns>
        bool IsRandom();
    }
}

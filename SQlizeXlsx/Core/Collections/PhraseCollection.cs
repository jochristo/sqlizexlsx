﻿using SQlizeXlsx.Linq2Excel;
using System.Threading.Tasks;
using SQlizeXlsx.Core.Domain;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Loads and holds a collection of <see cref="PhraseDTO"/> objects loaded from given Excel file.
    /// </summary>
    public class PhraseCollection : DataCollection<PhraseDTO>
    {
        private string _filename = string.Empty;
        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }

        /// <summary>
        /// Initializes the collection of <see cref="PhraseDTO"/> objects with given filename path.
        /// </summary>
        /// <param name="size"></param>
        public PhraseCollection(string filename) : base()
        {            
            this.Filename = filename;           
        }
                
        public override bool IsRandom()
        {
            return false;
        }

        /// <summary>
        /// Returns a file-parsed collection of <see cref="PhraseDTO"/> objects as a <see cref="ReferenceStorage{string, PhraseDTO}"/> object.
        /// The Value property of <see cref="ReferenceStorage{string, PhraseDTO}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{string, PhraseDTO}"/> instance holds an enumeration of <see cref="PhraseDTO"/> objects.
        /// </summary>
        /// <returns></returns>
        public async override Task<ReferenceStorage<string, PhraseDTO>> GetDataAsync()
        {
            var data = await SQLize.ScriptWithResultAsync<PhraseDTO>(this.Filename);
            this.Source = data.Data;            
            return data;
        }
    }
}

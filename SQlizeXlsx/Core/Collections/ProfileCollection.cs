﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQlizeXlsx.Linq2Excel;
using static SQlizeXlsx.Linq2Excel.Validation;
using SQlizeXlsx.Core.Domain;
using SQlizeXlsx.Core.Randomized;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Holds a fixed-size collection of <see cref="ProfileDTO"/> objects randomly generated.
    /// </summary>
    public class ProfileCollection : DataCollection<ProfileDTO>
    {        
        /// <summary>
        /// Size of collection.
        /// </summary>
        private int _size = 0;

        public ProfileCollection() : base()
        {

        }

        /// <summary>
        /// Initializes the collection of random <see cref="ProfileDTO"/> objects with given size.
        /// </summary>
        /// <param name="size"></param>
        public ProfileCollection(int size) : base()
        {
            this.Size = size;
            /*
            ProfileDTO profileDTO = new ProfileDTO();
            ReferenceKeeper<string, ProfileDTO> referenceKeeper = new ReferenceKeeper<string, ProfileDTO>();
            var iMappable = (IMappable)profileDTO;
            var data = new List<ProfileDTO>();
            for (var k = 0; k < size; k++)
            {
                data.Add(new ProfileDTO());
            }
            var random = Randomizer.Randomize<ProfileDTO>(data); // randomize field values
            var results = new List<Result>();
            var i = 1; // start at 1 because of excel header row!
            random.ToList().ForEach(d =>
            {
                var isValid = d.IsValid(i);
                if (!isValid)
                {
                    var result = d.Validate(i);
                    results.Add(result);
                }
                i++;
            });
            // ERRORS FOUND
            if (results.Count > 0)
            {
                var message = "[" + iMappable.Name() + "]\n";
                results.ForEach(m =>
                {
                    message += m.Message + "\n";
                });

                referenceKeeper.Value = message;
                referenceKeeper.IsValid = false;
                this._referenceKeeper =  referenceKeeper;
            }
            // Store actual list data and SQL insert string to ref instance
            referenceKeeper.Value = SQLize.JoinSqlInserts(random);
            referenceKeeper.IsValid = true;
            referenceKeeper.Data = random;
            this._referenceKeeper =  referenceKeeper;
            this.Source = data;
            */
        }

        /// <summary>
        /// Gets or sets the size of the collection.
        /// </summary>
        public int Size
        {
            get
            {
                return _size;
            }

            set
            {
                _size = value;
            }
        }

        /// <summary>
        /// Returns a random collection of <see cref="ProfileDTO"/> objects as a <see cref="ReferenceStorage{T, D}"/> object.
        /// The Value property of <see cref="ReferenceStorage{T, D}"/> instance holds the SQL-strignified insert statement.
        /// The Source property of <see cref="ReferenceStorage{T, D}"/> instance holds an enumeration of <see cref="ProfileDTO"/> objects.
        /// </summary>
        /// <returns></returns>
        public override Task<ReferenceStorage<string, ProfileDTO>> GetDataAsync()
        {
            if(this.Size == 0)
            {
                throw new ArgumentException("Size of random sequences is zero.");
            }

            return Task.Run(() =>
            {
                ProfileDTO profileDTO = new ProfileDTO();
                ReferenceStorage<string, ProfileDTO> referenceKeeper = new ReferenceStorage<string, ProfileDTO>();
                var iMappable = (IMappable)profileDTO;
                var data = new List<ProfileDTO>();
                for (var k = 0; k < this.Size; k++)
                {
                    data.Add(new ProfileDTO());
                }
                var random = Randomizer.Randomize<ProfileDTO>(data); // randomize field values
                var results = new List<Result>();
                var i = 1; // start at 1 because of excel header row!
                random.ToList().ForEach(d =>
                {
                    var isValid = d.IsValid(i);
                    if (!isValid)
                    {
                        var result = d.Validate(i);
                        results.Add(result);
                    }
                    i++;
                });
                // ERRORS FOUND
                if (results.Count > 0)
                {
                    var message = "[" + iMappable.Name() + "]\n";
                    results.ForEach(m =>
                    {
                        message += m.Message + "\n";
                    });

                    referenceKeeper.Value = message;
                    referenceKeeper.IsValid = false;                    
                }
                // Store actual list data and SQL insert string to ref instance
                referenceKeeper.Value = SQLize.JoinSqlInserts(random);
                referenceKeeper.IsValid = true;
                referenceKeeper.Data = random;                
                this.Source = data;
                return referenceKeeper;
            });
        }

        public override bool IsRandom()
        {
            return true;
        }
    }
}

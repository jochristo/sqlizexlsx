﻿using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Core
{
    /// <summary>
    /// Contains extention methods to several type instances.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Returns all messages contained in the list of <see cref="SQlizeXlsx.Sql.Models.Validation.Result"/> objects.
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public static string Messages(this List<Result> results)
        {
            var message = string.Empty;
            results.ForEach(r =>
            {
                message += r.Message + "\n";
            });
            return message;
        }

        /// <summary>
        /// Validates input object.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValid(this IValidatable input, int row)
        {
            return input.ValidationResult(row).Success;
        }

        /// <summary>
        /// Returns validation result message for given object.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ValidationMessage(this IValidatable input, int row)
        {
            return input.ValidationResult(row).Message;
        }

        /// <summary>
        /// Extension method to return validation result for given input object.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Result ValidationResult(this IValidatable input, int row)
        {
            // Avoids null check during validation of nullable objects
            return input == null ? new Result(false, "Null input is invalid.") : input.Validate(row);
        }

        /// <summary>
        /// Extension method to enumerate errors and return validation result for given input object.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Result ToValidationResult(this IEnumerable<string> input)
        {
            if (input == null)
            {
                return new Result(false, "Null input is invalid.");
            }
            // Enumerate the errors
            var errors = input.ToList();
            var success = !errors.Any();
            var message = success ? "Validation successful." : string.Join("\n", errors);
            return new Result(success, message);
        }

        /// <summary>
        /// Checks for null or empty or whitespace altogether.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNullOrEmptyOrWhiteSpace(this string input)
        {
            return string.IsNullOrEmpty(input) && string.IsNullOrWhiteSpace(input);
        }

        /// <summary>
        /// Finds occurences of given string value inside source string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int Occurences(this string str, string val)
        {
            int occurrences = 0;
            int startingIndex = 0;

            while ((startingIndex = str.IndexOf(val, startingIndex)) >= 0)
            {
                ++occurrences;
                ++startingIndex;
            }

            return occurrences;
        }
    }
}

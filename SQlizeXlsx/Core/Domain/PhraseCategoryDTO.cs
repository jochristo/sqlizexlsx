﻿using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Core.Domain
{
    /// <summary>
    /// Represents a PhraseCategory Excel row-mapped data storage and transfer object.
    /// </summary>
    public class PhraseCategoryDTO : IValidatable, IMappable, ISqlScript
    {
        private string _FK_category_id;
        private string _FK_phrase_id;
        private string _is_active = "1";

        public string FK_category_id
        {
            get
            {
                return _FK_category_id;
            }

            set
            {
                _FK_category_id = value.Trim();
            }
        }

        public string FK_phrase_id
        {
            get
            {
                return _FK_phrase_id;
            }

            set
            {
                _FK_phrase_id = value.Trim();
            }
        }

        public string is_active
        {
            get
            {
                return _is_active;
            }

            set
            {
                _is_active = "1";
            }
        }

        public virtual IEnumerable<string> PropertyNames()
        {
            var properties = this.GetType().GetProperties();
            var names = (from p in properties select p.Name.ToUpper());
            return names;
        }

        public virtual IEnumerable<string> ColumnNames()
        {
            var names = new List<string>();
            names.Add("FK_category_id".ToUpper());
            names.Add("FK_phrase_id".ToUpper());
            names.Add("is_active".ToUpper());
            return names.AsEnumerable();
        }

        public virtual Dictionary<string, string> Mappings()
        {
            var mappings = new Dictionary<string, string>();
            var names = PropertyNames();
            var columns = ColumnNames();
            var i = 0;
            names.ToList().ForEach(t =>
            {
                mappings.Add(t, columns.ElementAt(i));
                i++;
            });

            return mappings;
        }

        public string InsertPrefix()
        {
            return Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL_PREFIX;
        }

        public virtual Result Validate(int row)
        {
            var errors = new List<String>();
            // ignore first row of headers in excel
            row++;

            //start properties validation
            errors.Add("Row [" + row + "] :");

            if (FK_category_id == null || FK_category_id.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("FK_category_id value value must not be empty or whitespace string.");
            }
            if (FK_phrase_id == null || FK_phrase_id.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("FK_phrase_id value value must not be empty or whitespace string.");
            }

            if (errors.Count == 1)
            {
                errors.Clear();
            }
            return errors.ToValidationResult();
        }
        
        public virtual string Name()
        {
            return "Phrase-Categories";
        }

    }
}

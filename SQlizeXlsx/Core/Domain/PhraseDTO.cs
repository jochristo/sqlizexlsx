﻿using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using static SQlizeXlsx.Core.Attributes;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Core.Domain
{
    /// <summary>
    /// Represents a Phrase Excel row-mapped data storage and transfer object.
    /// </summary>
    public class PhraseDTO : IValidatable, IMappable, ISqlScript
    {
        private string _ph_id = null;
        [RandomValue(ElementAttributeType.DateTime)]
        private string _ph_created;
        private string _ph_deleted = "0";
        private string _ph_is_dummy = "1";
        private string _ph_gaps;
        private string _ph_isAdminAdded = "1";
        private string _ph_isApprovalPending = "0";
        private string _ph_phrase;
        private string _ph_profile_id;

        public string ph_id
        {
            get
            {
                return _ph_id;
            }

            set
            {
                _ph_id = value;
                _ph_id = null;
            }
        }

        public string ph_created
        {
            get
            {
                return _ph_created;
            }

            set
            {
                _ph_created = value;
                if (value != null)
                {
                    _ph_created = value.Trim();
                }
            }
        }

        public string ph_deleted
        {
            get
            {
                return _ph_deleted;
            }

            set
            {
                _ph_deleted = value;
                _ph_deleted = "0";
            }
        }

        public string ph_is_dummy
        {
            get
            {
                return _ph_is_dummy;
            }

            set
            {
                _ph_is_dummy = value;
                _ph_is_dummy = "1";
            }
        }

        public string ph_gaps
        {
            get
            {
                return _ph_gaps;
            }

            set
            {
                _ph_gaps = value;
                if (value != null)
                {
                    _ph_gaps = value.Trim();
                }
            }
        }

        public string ph_isAdminAdded
        {
            get
            {
                return _ph_isAdminAdded;
            }

            set
            {
                _ph_isAdminAdded = value;
                _ph_isAdminAdded = "1";
            }
        }

        public string ph_isApprovalPending
        {
            get
            {
                return _ph_isApprovalPending;
            }

            set
            {
                _ph_isApprovalPending = value;
                _ph_isApprovalPending = "0";
            }
        }

        public string ph_phrase
        {
            get
            {
                return _ph_phrase;
            }

            set
            {
                _ph_phrase = value;
                if (value != null)
                {
                    _ph_phrase = value.Trim();
                }
            }
        }

        public string ph_profile_id
        {
            get
            {
                return _ph_profile_id;
            }

            set
            {
                _ph_profile_id = value;
                _ph_profile_id = null;
            }
        }
        
        public virtual Result Validate(int row)
        {
            var errors = new List<String>();
            const string DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
            const string PHRASE_PATTERN = @"(^\s*[^\[\]\s]+(\s+[^\[\]\s]+)*(\s+\[\])+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)|(^\s*(\[\])(\s+\[\])*(\s+[^\[\]\s]+)+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)";
            DateTime dt;

            // ignore first row of headers in excel
            row++;

            //start properties validation
            errors.Add("Row [" + row + "] :");

            bool isDateTimeValid = DateTime.TryParseExact(ph_created, DATETIME_PATTERN, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateTimeValid || ph_created.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("ph_created value must not be empty or whitespace string and in the format 'yyyy-MM-dd HH:mm:ss'.");
            }

            if (ph_phrase.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("ph_phrase value must not be empty or whitespace string.");
            }
            else
            {
                // trim first
                var trimmed = ph_phrase.Trim();
                //replace double whitespace with one
                var firstPass = Regex.Replace(trimmed, @"\s{2,}", " ");
                var secondPass = Regex.Replace(firstPass, @"\[\s+\]", "[]");
                if (!Regex.IsMatch(secondPass, PHRASE_PATTERN))
                {
                    errors.Add("ph_phrase must be in the format: [] [] [] word [] word word etc. or WORD [] WORD WORD [] [] etc.");
                    ph_phrase = secondPass;
                }
                else
                {
                    var finalPass = secondPass.Replace("'", "\\'");
                    ph_phrase = finalPass;

                    // set gaps based on occurrences of []
                    this.ph_gaps = ph_phrase.Occurences("[]").ToString();
                }
            }

            if (errors.Count == 1)
            {
                errors.Clear();
            }
            return errors.ToValidationResult();
        }

        public virtual IEnumerable<string> PropertyNames()
        {
            var properties = this.GetType().GetProperties();
            var names = (from p in properties select p.Name.ToUpper());
            return names;
        }

        public virtual IEnumerable<string> ColumnNames()
        {
            var names = new List<string>();
            names.Add("ph_id".ToUpper());
            names.Add("ph_created".ToUpper());
            names.Add("ph_deleted".ToUpper());
            names.Add("pr_is_dummy".ToUpper());
            names.Add("ph_gaps".ToUpper());
            names.Add("ph_isAdminAdded".ToUpper());
            names.Add("ph_isApprovalPending".ToUpper());
            names.Add("ph_phrase".ToUpper());
            names.Add("ph_profile_id".ToUpper());
            return names.AsEnumerable();
        }

        public virtual Dictionary<string, string> Mappings()
        {
            var mappings = new Dictionary<string, string>();
            var names = PropertyNames();
            var columns = ColumnNames();
            var i = 0;
            names.ToList().ForEach(t =>
            {
                mappings.Add(t, columns.ElementAt(i));
                i++;
            });

            return mappings;
        }

        public virtual string InsertPrefix()
        {
            return Linq2Excel.SqlScript.PHRASE_INSERT_SQL_PREFIX;
        }

        public virtual string Name()
        {
            return "Phrases";
        }

    }
}

﻿using SQlizeXlsx.Core.Generators;
using SQlizeXlsx.Core.Randomized;
using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using static SQlizeXlsx.Core.Attributes;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Core.Domain
{
    /// <summary>
    /// Represents an Answer Excel row-mapped data storage and transfer object.
    /// </summary>
    public class AnswerDTO : IValidatable, IMappable, ISqlScript
    {
        private string _an_id = null;
        private string _an_answers;
        [RandomValue(ElementAttributeType.DateTime)]
        private string _an_created;
        private string _an_deleted = "0";
        private string _an_is_dummy = "1";
        [RandomValue(ElementAttributeType.Likes)]
        private string _an_likes;
        private string _an_reports = "0";
        private string _an_phrase_id;
        [RandomValue(ElementAttributeType.ForeignKey)]
        private string _an_profile_id = null;

        public string an_id
        {
            get
            {
                return _an_id;
            }

            set
            {
                _an_id = null;
            }
        }

        public string an_answers
        {
            get
            {
                return _an_answers;
            }

            set
            {
                _an_answers = value;
                if (value != null)
                {
                    _an_answers = value.Trim();
                }
            }
        }

        public string an_created
        {
            get
            {
                return _an_created;
            }

            set
            {
                _an_created = value;
                if (value != null)
                {
                    _an_created = value.Trim();
                }
            }
        }

        public string an_deleted
        {
            get
            {
                return _an_deleted;
            }

            set
            {
                _an_deleted = "0";
            }
        }

        public string an_is_dummy
        {
            get
            {
                return _an_is_dummy;
            }

            set
            {
                _an_is_dummy = "1";
            }
        }

        public string an_likes
        {
            get
            {
                return _an_likes;
            }

            set
            {
                _an_likes = new NumberGenerator().NextInteger(1, 25).ToString().Trim();
                //_an_likes = value.Trim();
            }
        }

        public string an_reports
        {
            get
            {
                return _an_reports;
            }

            set
            {
                _an_reports = "0";
            }
        }

        public string an_phrase_id
        {
            get
            {
                return _an_phrase_id;
            }

            set
            {
                _an_phrase_id = value;
                if (value != null)
                {
                    _an_phrase_id = value.Trim();
                }

            }
        }

        public string an_profile_id
        {
            get
            {
                return _an_profile_id;
            }

            set
            {
                _an_profile_id = value;
                if (value != null)
                {
                    _an_profile_id = value.Trim();
                }
            }
        }

        public bool validate(out string message)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<string> PropertyNames()
        {
            var properties = this.GetType().GetProperties();
            var names = (from p in properties select p.Name.ToUpper());
            return names;
        }

        public virtual IEnumerable<string> ColumnNames()
        {
            var names = new List<string>();
            names.Add("an_id".ToUpper());
            names.Add("an_answers".ToUpper());
            names.Add("an_created".ToUpper());
            names.Add("an_deleted".ToUpper());
            names.Add("an_is_dummy".ToUpper());
            names.Add("an_likes".ToUpper());
            names.Add("an_reports".ToUpper());
            names.Add("an_phrase_id".ToUpper());
            names.Add("an_profile_id".ToUpper());
            return names.AsEnumerable();
        }

        public virtual Dictionary<string, string> Mappings()
        {
            var mappings = new Dictionary<string, string>();
            var names = PropertyNames();
            var columns = ColumnNames();
            var i = 0;
            names.ToList().ForEach(t =>
            {
                mappings.Add(t, columns.ElementAt(i));
                i++;
            });

            return mappings;
        }

        public virtual Result Validate(int row)
        {
            var errors = new List<String>();
            const string DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
            const string ANSWER_PATTERN = @"^\s*\[\s*({\s*""[w][o][r][d]""\s*:\s*""\s*[^\s\[\]""]+(\s*[^\s\[\]""]+\s*)*""\s*}\s*){1}(,\s*{\s*""[w][o][r][d]""\s*:\s*""\s*[^\s\[\]""]+(\s*[^\s\[\]""]+\s*)*""\s*}\s*)*\]\s*$";
            DateTime dt;
            int result;

            // ignore first row of headers in excel
            row++;

            //start properties validation
            errors.Add("Row [" + row + "] :");

            if (an_answers.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("an_answers value must not be empty or whitespace string.");
            }
            else
            {
                // trim first
                var trimmed = an_answers.Trim();
                var firstPass = Regex.Replace(trimmed, @"\s{2,}", " ");
                if (!Regex.IsMatch(firstPass, ANSWER_PATTERN))
                {
                    errors.Add("an_answers must be in the format: [{\"word\":\"words words\"},{...},{...}]");
                    an_answers = firstPass;
                }
                else
                {
                    var finalPass = firstPass.Replace("'", "\\'");
                    an_answers = finalPass;
                }
            }

            bool isDateTimeValid = DateTime.TryParseExact(an_created, DATETIME_PATTERN, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateTimeValid || an_created.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("an_created value must not be empty or whitespace string and in the format 'yyyy-MM-dd HH:mm:ss'.");
            }

            bool isNumber = Int32.TryParse(an_likes, out result);
            if (!isNumber)
            {
                errors.Add("an_likes value must not be empty or whitespace string and >= 0.");
            }
            else
            {
                if (result < 0)
                {
                    errors.Add("an_likes value must be >= 0.");
                }
            }

            isNumber = Int32.TryParse(an_phrase_id, out result);
            if (!isNumber)
            {
                errors.Add("an_phrase_id value must not be empty or whitespace string and greater than zero.");
            }
            else
            {
                if (result <= 0)
                {
                    errors.Add("an_phrase_id value must be greater than zero.");
                }
            }

            if (errors.Count == 1)
            {
                errors.Clear();
            }
            return errors.ToValidationResult();
        }

        public virtual string InsertPrefix()
        {
            return SqlScript.ANSWER_INSERT_SQL_PREFIX;
        }

        public virtual string Name()
        {
            return "Answers";
        }
    }
}

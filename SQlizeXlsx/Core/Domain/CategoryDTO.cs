﻿using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using static SQlizeXlsx.Linq2Excel.Validation;
using static SQlizeXlsx.Core.Attributes;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SQlizeXlsx.Core.Domain
{
    /// <summary>
    /// Represents a Category Excel row-mapped data storage and transfer object.
    /// </summary>
    public class CategoryDTO : IValidatable, IMappable, ISqlScript
    {
        private string _cat_id = null;
        [RandomValue(ElementAttributeType.DateTime)]
        private string _cat_created;
        private string _cat_deleted = "0";
        private string _cat_description;
        private string _cat_img_rel_path;
        private string _cat_name;
        private string _culture_id;

        public string cat_id
        {
            get
            {
                return _cat_id;
            }

            set
            {
                _cat_id = value;
                _cat_id = null;
            }
        }

        public string cat_created
        {
            get
            {
                return _cat_created;
            }

            set
            {
                _cat_created = value.Trim();
            }
        }

        public string cat_deleted
        {
            get
            {
                return _cat_deleted;
            }

            set
            {
                _cat_deleted = value;
                _cat_deleted = "0";
            }
        }

        public string cat_description
        {
            get
            {
                return _cat_description;
            }

            set
            {
                _cat_description = value.Trim();
            }
        }

        public string cat_img_rel_path
        {
            get
            {
                return _cat_img_rel_path;
            }

            set
            {
                _cat_img_rel_path = value.Trim();
            }
        }

        public string cat_name
        {
            get
            {
                return _cat_name;
            }

            set
            {
                _cat_name = value.Trim();
            }
        }

        public string culture_id
        {
            get
            {
                return _culture_id;
            }

            set
            {
                _culture_id = value.Trim();
            }
        }

        public virtual Result Validate(int row)
        {
            var errors = new List<String>();
            var dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            DateTime dt;
            int result;
            row++;
            //start properties validation
            errors.Add("Row [" + row + "] :");

            bool isDateTimeValid = DateTime.TryParseExact(cat_created, dateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateTimeValid || cat_created.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("cat_created value must not be empty or whitespace string and in the format 'yyyy-MM-dd HH:mm:ss'.");
            }

            if (!cat_img_rel_path.IsNullOrEmptyOrWhiteSpace())
            {
                //check against regex
                var regex = new Regex(@"^/[A-Za-z0-9_-]+\.([pP][nN][gG]$)|([jJ][pP][gG]$)|([gG][iI][fF]$)");
                if (!regex.IsMatch(cat_img_rel_path))
                {
                    errors.Add("cat_img_rel_path value be in the format name : '/[name].[png][gif][jpg]'");
                }
            }

            if (cat_name.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("cat_name value must not be empty or whitespace string.");
            }

            var isNumber = Int32.TryParse(culture_id, out result);
            if (!isNumber)
            {
                errors.Add("culture_id value must not be empty or whitespace string.");
            }
            else
            {
                if (result <= 0)
                {
                    errors.Add("culture_id value must be between 1 and 136.");
                }
            }
            if (errors.Count == 1)
            {
                errors.Clear();
            }
            return errors.ToValidationResult();
        }

        public virtual IEnumerable<string> PropertyNames()
        {
            var properties = this.GetType().GetProperties();
            var names = (from p in properties select p.Name.ToUpper());
            return names;
        }

        public virtual  IEnumerable<string> ColumnNames()
        {
            var names = new List<string>();
            names.Add("cat_id".ToUpper());
            names.Add("cat_created".ToUpper());
            names.Add("cat_deleted".ToUpper());
            names.Add("cat_description".ToUpper());
            names.Add("cat_img_rel_path".ToUpper());
            names.Add("cat_name".ToUpper());
            names.Add("culture_id".ToUpper());
            return names.AsEnumerable();
        }

        public virtual Dictionary<string, string> Mappings()
        {
            var mappings = new Dictionary<string, string>();
            var names = PropertyNames();
            var columns = ColumnNames();
            var i = 0;
            names.ToList().ForEach(t =>
            {
                mappings.Add(t, columns.ElementAt(i));
                i++;
            });

            return mappings;
        }

        public virtual string InsertPrefix()
        {
            return Linq2Excel.SqlScript.CATEGORY_INSERT_SQL_PREFIX;
        }

        public virtual string Name()
        {
            return "Categories";
        }
    }
}

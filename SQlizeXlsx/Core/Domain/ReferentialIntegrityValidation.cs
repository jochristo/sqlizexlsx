﻿using Newtonsoft.Json;
using SQlizeXlsx.Core;
using SQlizeXlsx.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Domain
{
    /// <summary>
    /// Contains methods to validate excel-mapped data storage and transfer objects against referential integrity rules and data consistency.
    /// </summary>
    public class ReferentialIntegrityValidation
    {
        // Internal storage of data
        private IEnumerable<PhraseDTO> _phrases = new List<PhraseDTO>();
        private IEnumerable<AnswerDTO> _answers = new List<AnswerDTO>();
        private IEnumerable<ProfileDTO> _profiles = new List<ProfileDTO>();
        private IEnumerable<CategoryDTO> _categories = new List<CategoryDTO>();
        private IEnumerable<PhraseCategoryDTO> _phraseCategories = new List<PhraseCategoryDTO>();
        
        // referential integrity validation results
        private List<Result> _results = new List<Result>();

        // internal storage for duplicates checks
        private HashSet<string> _nicknames = new HashSet<string>();
        private HashSet<string> _emails = new HashSet<string>();
        private HashSet<string> _uniques = new HashSet<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferentialIntegrityValidation"/> class with the default constructor.
        /// </summary>
        public ReferentialIntegrityValidation() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferentialIntegrityValidation"/> class with the source enumerations of <see cref="PhraseDTO"/>, <see cref="AnswerDTO"/>, and <see cref="ProfileDTO"/> objects.
        /// </summary>
        /// <param name="phrases"></param>
        /// <param name="answers"></param>
        /// <param name="profiles"></param>
        public ReferentialIntegrityValidation(IEnumerable<PhraseDTO> phrases, IEnumerable<AnswerDTO> answers, IEnumerable<ProfileDTO> profiles )
        {
            _phrases = phrases;
            _answers = answers;
            _profiles = profiles;           
        }

        /// <summary>
        /// Validates internal storage and transfer enumerated collections against specific referential integrity and data consistency rules.
        /// Updates the list of <see cref="Result"/> objects if validation fails.
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            // Validate Phrases
            var phrasesList = this.Phrases.ToList();
            phrasesList.AsParallel().WithDegreeOfParallelism(4).ToList().ForEach(p => 
            {
                var currentIndex = phrasesList.IndexOf(p);
                var row = currentIndex + 2; // excel row
                if (!p.ph_profile_id.IsNullOrEmptyOrWhiteSpace())
                {
                    var value = 0;
                    var parsed = Int32.TryParse(p.ph_profile_id, out value);
                    if (parsed)
                    {
                        var profileIndex = Int32.Parse(p.ph_profile_id) - 1; // profile list row
                        var profile = Profiles.ElementAtOrDefault(profileIndex);
                        if (profile == null)
                        {
                            Result result = new Result(false, "Inconsistent ph_profile_id reference <" + p.ph_profile_id + "> found in phrases data file at row [" + row + "].");
                            Results.Add(result);
                        }
                    }
                }
            });            

            // Validate Answers         
            var answersList = this.Answers.ToList();
            answersList.AsParallel().WithDegreeOfParallelism(4).ToList().ForEach(a=>
            {
                var currentIndex = answersList.IndexOf(a);
                var row = currentIndex + 2; // excel row

                var value = 0;
                var parsed = Int32.TryParse(a.an_phrase_id, out value);
                int phraseIndex = -1;
                if (parsed)
                {
                    phraseIndex = value - 1;
                }
                else
                {
                    //phraseIndex = Int32.Parse(a.an_phrase_id) - 1; // phrase list row 
                    phraseIndex = -1; 
                }
                              
                parsed = Int32.TryParse(a.an_profile_id, out value);
                int profileIndex = -1;
                if (parsed)
                {
                    profileIndex = value - 1;
                }
                else
                {
                    //phraseIndex = Int32.Parse(a.an_phrase_id) - 1; // phrase list row 
                    profileIndex = -1;
                }
                //var profileIndex = Int32.Parse(a.an_profile_id) - 1; // profile list row

                var phrase = Phrases.ElementAtOrDefault(phraseIndex);
                var profile = Profiles.ElementAtOrDefault(profileIndex);
                if(phrase == null)
                {
                    Result result = new Result(false,"Inconsistent an_phrase_id reference <"+ a.an_phrase_id +"> found in answers data file at row ["+row+"].");
                    Results.Add(result);
                }
                if(profile == null)
                {
                    Result result = new Result(false, "Inconsistent an_profile_id reference <" + a.an_profile_id + "> found in answers data file at row [" + row + "].");
                    Results.Add(result);
                }               
                
                //check if phrase gaps match answer json string 'word' count
                if (phrase != null)
                {
                    var jsonAnswer = a.an_answers;
                    var message = string.Empty;
                    var jsonValidationResult = JsonValidationResult.IsJson;
                    bool isValid = isJsonAnswer(jsonAnswer, out jsonValidationResult, out message);
                    if (!isValid)
                    {
                        Result result = new Result(false, "Invalid json answer format: <" + jsonAnswer + "> at row [" + row + "].");
                        Results.Add(result);
                    }
                    else
                    {
                        if (jsonValidationResult == JsonValidationResult.EmptyJson)
                        {
                            Result result = new Result(false, "Invalid json answer format: <" + jsonAnswer + "> at row [" + row + "].");
                            Results.Add(result);
                        }
                        else if (jsonValidationResult == JsonValidationResult.IsJson)
                        {
                            var trimmed = jsonAnswer.Trim();
                            AnswerJson[] answerJson = JsonConvert.DeserializeObject<AnswerJson[]>(trimmed);
                            var words = answerJson.Length;
                            var gaps = Int32.Parse(phrase.ph_gaps);
                            if (words != gaps)
                            {
                                var error = string.Empty;
                                error = "Inconsistent pair of an_answers and an_phrase_id found in answers data file at row [" + row + "] :";
                                error += "Phrase gaps: " + gaps + " - Answer words: " + words + ".";
                                Result result = new Result(false, error);
                                Results.Add(result);
                            }
                        }
                    }
                }

            });

            // Validate Profiles
            var profilesList = this.Profiles.ToList();
            profilesList.AsParallel().WithDegreeOfParallelism(4).ToList().ForEach(p =>
            {
                //check for duplicate nicknames and emails
                var currentIndex = profilesList.IndexOf(p);
                var row = currentIndex + 2; // excel row
                if (_nicknames.Contains(p.pr_nickname))
                {
                    Result result = new Result(false, "Duplicate pr_nickname <" + p.pr_nickname + "> found in profiles data file at row [" + row + "].");
                    Results.Add(result);
                }
                else
                {
                    _nicknames.Add(p.pr_nickname);
                }

                if (_emails.Contains(p.pr_email))
                {
                    Result result = new Result(false, "Duplicate pr_email <" + p.pr_email + "> found in profiles data file at row [" + row + "].");
                    Results.Add(result);
                }
                else
                {
                    _emails.Add(p.pr_email);
                }
            });

            // Validate Categories
            var categoriesList = this.Categories.ToList();
            _uniques.Clear();
            categoriesList.AsParallel().WithDegreeOfParallelism(4).ToList().ForEach(c =>
            {
                var currentIndex = categoriesList.IndexOf(c);
                var row = currentIndex + 2; // excel row
                if (_uniques.Contains(c.cat_name))
                {
                    Result result = new Result(false, "Duplicate cat_name <" + c.cat_name + "> found in categories data file at row [" + row + "].");
                    Results.Add(result);
                }
                else
                {
                    _uniques.Add(c.cat_name);
                }

            });

            // Validate Phrase-categories pairs for inconsistency and duplicates
            //ValidatePhraseCatefories(this.PhraseCategories); //enable this line if data is coming from data file

            return Results.Count() == 0;
        }

        /// <summary>
        /// Validates phrase and category collection for referential integrity.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private bool ValidatePhraseCategories(IEnumerable<PhraseCategoryDTO> source)
        {
            _uniques.Clear();
            var list = source.ToList();
            var phrases = Phrases.ToList();
            var categories = Categories.ToList();
            var results = new List<Result>();
            list.ForEach(i =>
            {
                var currentIndex = list.IndexOf(i);
                var row = currentIndex + 2; // excel row
                var phraseIndex = Int32.Parse(i.FK_phrase_id) - 1; // phrase list row
                var categoryIndex = Int32.Parse(i.FK_category_id) - 1; // profile list row
                var phrase = phrases.ElementAtOrDefault(phraseIndex);
                var category = categories.ElementAtOrDefault(categoryIndex);
                if (phrase == null)
                {
                    Result result = new Result(false, "Inconsistent FK_phrase_id reference <" + i.FK_phrase_id + "> found in phrase_category data file at row [" + row + "].");
                    results.Add(result);
                    Results.Add(result);
                }
                if (category == null)
                {
                    Result result = new Result(false, "Inconsistent FK_category_id reference <" + i.FK_category_id + "> found in phrase_category data file at row [" + row + "].");
                    results.Add(result);
                    Results.Add(result);
                }

                // check for duplicate pairs
                var pair = i.FK_category_id + i.FK_phrase_id;
                if (_uniques.Contains(pair))
                {
                    Result result = new Result(false, "Duplicate FK_category_id-FK_phrase_id pair <" + i.FK_category_id + " -  " + i.FK_phrase_id + "> found in phrase_category data file at row [" + row + "].");
                    results.Add(result);
                    Results.Add(result);
                }
                else
                {
                    _uniques.Add(pair);
                }

            });
            return results.Count() == 0;
        }

        /// <summary>
        /// Gets or sets the enumerated collection of <see cref="PhraseDTO"/> objects to be validated.
        /// </summary>
        public IEnumerable<PhraseDTO> Phrases
        {
            get
            {
                return _phrases;
            }

            set
            {
                _phrases = value;
            }
        }

        /// <summary>
        /// Gets or sets the enumerated collection of <see cref="AnswerDTO"/> objects to be validated.
        /// </summary>
        public IEnumerable<AnswerDTO> Answers
        {
            get
            {
                return _answers;
            }

            set
            {
                _answers = value;
            }
        }

        /// <summary>
        /// Gets or sets the enumerated collection of <see cref="ProfileDTO"/> objects to be validated.
        /// </summary>
        public IEnumerable<ProfileDTO> Profiles
        {
            get
            {
                return _profiles;
            }

            set
            {
                _profiles = value;
            }
        }

        /// <summary>
        /// Gets or sets the collection list of <see cref="Result"/> objects for the relational validation check.
        /// </summary>
        public List<Result> Results
        {
            get
            {
                return _results;
            }

            set
            {
                _results = value;
            }
        }

        /// <summary>
        /// Gets or sets the enumerated collection of <see cref="PhraseCategoryDTO"/> objects to be validated.
        /// </summary>
        public IEnumerable<PhraseCategoryDTO> PhraseCategories
        {
            get
            {
                return _phraseCategories;
            }

            set
            {
                _phraseCategories = value;
            }
        }

        /// <summary>
        /// Gets or sets the enumerated collection of <see cref="CategoryDTO"/> objects to be validated.
        /// </summary>
        public IEnumerable<CategoryDTO> Categories
        {
            get
            {
                return _categories;
            }

            set
            {
                _categories = value;
            }
        }
    }


}

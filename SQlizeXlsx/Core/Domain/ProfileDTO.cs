﻿using SQlizeXlsx.Core.Randomized;
using SQlizeXlsx.Linq2Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using static SQlizeXlsx.Core.Attributes;
using static SQlizeXlsx.Core.Randomized.Randomizer;
using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Core.Domain
{
    /// <summary>
    /// Represents a Profile Excel row-mapped data storage and transfer object.
    /// </summary>
    public partial class ProfileDTO : IValidatable, IMappable, ISqlScript
    {
        private string _pr_id;

        [RandomValue(ElementAttributeType.DateTime)]
        private string _pr_created;

        private string _pr_deleted = "0";

        [RandomValue(ElementAttributeType.Date)]
        private string _pr_dob;

        private string _pr_is_dummy = "1";

        [RandomValue(ElementAttributeType.Email)]
        private string _pr_email;

        [RandomValue(ElementAttributeType.Gender)]
        private string _pr_gender;

        private string _pr_isMobileNoVerified = "1";

        [RandomValue(ElementAttributeType.Culture)]
        private string _pr_language;

        [RandomValue(ElementAttributeType.Level)]
        private string _pr_level;

        [RandomValue(ElementAttributeType.Phone)]
        private string _pr_mobileNo;

        [RandomValue(ElementAttributeType.Nickname)]
        private string _pr_nickname;

        private string _pr_password = Hashing.Sha1Hash("whatever");

        private string _pr_picture_filepath;

        [RandomValue(ElementAttributeType.Long)]
        private string _pr_score;

        [RandomValue(ElementAttributeType.Longitude)]
        private string _pr_longitude;

        [RandomValue(ElementAttributeType.Latitude)]
        private string _pr_latitude;

        #region ProfileDTO properties

        public string pr_id
        {
            get
            {
                return _pr_id;
            }

            set
            {
                _pr_id = value;
                _pr_id = null;
            }
        }

        public string pr_created
        {
            get
            {
                return _pr_created;
            }

            set
            {
                _pr_created = value;
            }
        }

        public string pr_deleted
        {
            get
            {
                return _pr_deleted;
            }

            set
            {
                _pr_deleted = "0";
            }
        }

        public string pr_dob
        {
            get
            {
                return _pr_dob;
            }

            set
            {
                _pr_dob = value;
            }
        }

        public string pr_is_dummy
        {
            get
            {
                return _pr_is_dummy;
            }

            set
            {
                _pr_is_dummy = value;
                _pr_is_dummy = "1";
            }
        }

        public string pr_email
        {
            get
            {
                return _pr_email;
            }

            set
            {
                _pr_email = value;
            }
        }

        public string pr_gender
        {
            get
            {
                return _pr_gender;
            }

            set
            {
                _pr_gender = value;
            }
        }

        public string pr_isMobileNoVerified
        {
            get
            {
                return _pr_isMobileNoVerified;
            }

            set
            {
                _pr_isMobileNoVerified = value;
                _pr_isMobileNoVerified = "1";
            }
        }

        public string pr_language
        {
            get
            {
                return _pr_language;
            }

            set
            {
                _pr_language = value;
            }
        }

        public string pr_level
        {
            get
            {
                return _pr_level;
            }

            set
            {
                _pr_level = value;
                _pr_level = "5";
            }
        }

        [RandomValue(ElementAttributeType.Phone)]
        public string pr_mobileNo
        {
            get
            {
                return _pr_mobileNo;
            }

            set
            {
                _pr_mobileNo = value;
                //_pr_mobileNo = "+309999000000";
            }
        }

        public string pr_nickname
        {
            get
            {
                return _pr_nickname;
            }

            set
            {
                _pr_nickname = value;
            }
        }

        public string pr_password
        {
            get
            {
                return _pr_password;
            }

            set
            {
                _pr_password = "1000:blablablablabla";
            }
        }

        public string pr_picture_filepath
        {
            get
            {
                return _pr_picture_filepath;
            }

            set
            {
                _pr_picture_filepath = "";
            }
        }

        public string pr_score
        {
            get
            {
                return _pr_score;
            }

            set
            {
                _pr_score = value;
                //_pr_score = "500";
            }
        }
        [RandomValue(ElementAttributeType.Longitude)]
        public string pr_longitude
        {
            get
            {
                return _pr_longitude;
            }

            set
            {
                //var  temp = value.Replace(",",".");
                //_pr_longitude = temp;   
                _pr_longitude = value;
            }
        }
        [RandomValue(ElementAttributeType.Latitude)]
        public string pr_latitude
        {
            get
            {
                return _pr_latitude;
            }

            set
            {
                //var temp = value.Replace(",", ".");
                //_pr_latitude = temp;
                _pr_latitude = value;
            }
        }

        #endregion

        public virtual IEnumerable<string> PropertyNames()
        {
            var properties = this.GetType().GetProperties();
            var names = (from p in properties select p.Name.ToUpper());
            return names;
        }

        public virtual IEnumerable<string> ColumnNames()
        {
            var names = new List<string>();
            names.Add("pr_id".ToUpper());
            names.Add("pr_created".ToUpper());
            names.Add("pr_deleted".ToUpper());
            names.Add("pr_dob".ToUpper());
            names.Add("pr_is_dummy".ToUpper());
            names.Add("pr_email".ToUpper());
            names.Add("pr_gender".ToUpper());
            names.Add("pr_isMobileNoVerified".ToUpper());
            names.Add("pr_language".ToUpper());
            names.Add("pr_level".ToUpper());
            names.Add("pr_mobileNo".ToUpper());
            names.Add("pr_nickname".ToUpper());
            names.Add("pr_password".ToUpper());
            names.Add("pr_picture_filepath".ToUpper());
            names.Add("pr_score".ToUpper());
            names.Add("pr_longitude".ToUpper());
            names.Add("pr_latitude".ToUpper());
            return names.AsEnumerable();
        }

        public virtual Dictionary<string, string> Mappings()
        {
            var mappings = new Dictionary<string, string>();
            var names = PropertyNames();
            var columns = ColumnNames();
            var i = 0;
            names.ToList().ForEach(t =>
            {
                mappings.Add(t, columns.ElementAt(i));
                i++;
            });

            return mappings;
        }

        public virtual Result Validate(int row)
        {
            var errors = new List<String>();
            var dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            var dateFormat = "yyyy-MM-dd";
            var longitudePattern = @"^(^((-?([1-9]|([1-9][0-9])|([1][0-7][0-9]))|(([0-9]|([1-9][0-9])|([1][0-7][0-9]))))((\.|,)\d{1,7})?){1}$)|(^[1][8][0]((\.|,)[0]{1})?$)|(^-[1][8][0]((\.|,)[0]{1})?$)|(^[0]$)$";
            longitudePattern = @"^(^(((-?([1-9]|([1-9][0-9])|([1][0-7][0-9]))|(([0-9]|([1-9][0-9])|([1][0-7][0-9]))))((\.|,)\d{1,7})?)|(-[0]((\.|,)\d{1,7})+)){1}$)|(^[1][8][0]((\.|,)[0]{1})?$)|(^-[1][8][0]((\.|,)[0]{1})?$)|(^[0]$)$"; // allows -0.1 etc
            var latitudePattern = @"^(^((-?([1-9]|([1-8][0-9]))|(([0-8]|([1-8][0-9]))))((\.|,)\d{1,7})?){1}$)|(^[9][0]((\.|,)[0]{1})?$)|(^-[9][0]((\.|,)[0]{1})?$)|(^[0]$)$";
            latitudePattern = @"^(^(((-?([1-9]|([1-8][0-9]))|(([0-8]|([1-8][0-9]))))((\.|,)\d{1,7})?)|(-[0]((\.|,)\d{1,7})+)){1}$)|(^[9][0]((\.|,)[0]{1})?$)|(^-[9][0]((\.|,)[0]{1})?$)|(^[0]$)$"; // allows -0.1 etc
            DateTime dt;

            // ignore first row of headers in excel
            row++;

            //start properties validation
            errors.Add("Row [" + row + "] :");

            bool isDateTimeValid = DateTime.TryParseExact(pr_created, dateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateTimeValid || pr_created.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_created value must not be empty/whitespace string and in the format 'yyyy-MM-dd HH:mm:ss'.");
            }

            int result;
            bool isDateValid = DateTime.TryParseExact(pr_dob, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateValid || pr_dob.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_dob value must not be empty/whitespace string and in the format 'yyyy-MM-dd'.");
            }

            if (pr_email.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_email value must not be empty or whitespace string.");
            }
            else
            {
                if (!Regex.IsMatch(pr_email, @"^(.+)@(.+)$"))
                {
                    errors.Add("pr_email value is not a valid email address.");
                }
            }

            if (pr_gender.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_gender value must not be empty or whitespace string.");
            }
            else
            {
                if (!Regex.IsMatch(pr_gender, @"^([Mm][Aa][Ll][Ee])|([Ff][Ee][Mm][Aa][Ll][Ee])|([Oo][Tt][Hh][Ee][Rr])$"))
                {
                    errors.Add("pr_gender value must be either Male/Female/Other.");
                }
            }

            if (pr_language.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_language value must not be empty or whitespace string.");
            }

            if (pr_nickname.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_nickname value must not be empty or whitespace string.");
            }

            var isNumber = Int32.TryParse(pr_score, out result);
            if (!isNumber)
            {
                errors.Add("pr_score value must not be empty or whitespace string.");
            }
            else
            {
                if (result < 0)
                {
                    errors.Add("pr_score value must be >= 0.");
                }
            }

            if (pr_longitude.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_longitude value must not be empty or whitespace string.");
            }
            else
            {
                if (!Regex.IsMatch(pr_longitude, longitudePattern))
                {
                    errors.Add("pr_longitude must be between -180 and 180 degrees with max 7 decimal places");
                }
            }

            if (pr_latitude.IsNullOrEmptyOrWhiteSpace())
            {
                errors.Add("pr_latitude value must not be empty or whitespace string.");
            }
            else
            {
                if (!Regex.IsMatch(pr_latitude, latitudePattern))
                {
                    errors.Add("pr_latitude must be between -90 and 90 degrees with max 7 decimal places");
                }
            }


            if (errors.Count == 1)
            {
                errors.Clear();
            }
            return errors.ToValidationResult();
        }

        public virtual string InsertPrefix()
        {
            return Linq2Excel.SqlScript.PROFILE_INSERT_SQL_PREFIX;
        }

        public virtual string Name()
        {
            return "Profiles";
        }

    }
}

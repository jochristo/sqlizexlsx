﻿using SQlizeXlsx.Core.Randomized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Generator of integer values 0-9.
    /// </summary>
    public class DigitGenerator : IRandomType
    {
        private static char[] numChars = "0123456789".ToCharArray();
        public char[] alphabet()
        {
            return numChars;
        }
    }
}

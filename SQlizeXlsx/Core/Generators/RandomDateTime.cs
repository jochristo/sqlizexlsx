﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Datetime generator for datetime and date formats.
    /// </summary>
    public class RandomDateTime
    {
        private DateTime start;
        private Random generator = new Random();
        private int range;
        private static string dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        private static string dateFormat = "yyyy-MM-dd";

        /// <summary>
        /// Initializes a new instance of <see cref="RandomDateTime"/> class with default start date and range values.
        /// </summary>
        public RandomDateTime()
        {
            start = new DateTime(1995, 1, 1);
            range = (DateTime.Today - start).Days;
        }

        private DateTime NextDateTime()
        {
            start = new DateTime(2010, 1, 1);
            range = (DateTime.Today - start).Days;
            return GenererateRandomDate(start.Year, DateTime.Now.Year);
        }

        private DateTime NextDateTime(int year, int month, int day)
        {
            start = new DateTime(year, month, day);
            range = (DateTime.Today - start).Days;
            return GenererateRandomDate(start.Year, DateTime.Now.Year);
        }

        // For date of births  > 18yrs old from today
        private DateTime NextDate()
        {
            this.start = DateTime.Today;
            var high = start.AddYears(-18); // start at 18 yrs old
            var low = high.AddYears(-32); // go as back as 50 yrs max
            range = (high - low).Days;
            return GenererateRandomDate(low.Year, high.Year);
        }

        /// <summary>
        /// Returns a random datetime instance as a string value formatted based on the type given.
        /// </summary>
        /// <param name="datetimeType"></param>
        /// <returns></returns>
        public string NextToString(DatetimeType datetimeType)
        {
            string type = datetimeType == DatetimeType.DateTime ? dateTimeFormat : dateFormat;
            return type = datetimeType == DatetimeType.DateTime ? NextDateTime(2000, 1, 1).ToString(dateTimeFormat) : NextDate().ToString(dateFormat);
        }

        /// <summary>
        /// Represents the type of date-time format to use.
        /// </summary>
        public enum DatetimeType
        {
            DateTime, Date
        }

        /// <summary>
        /// Generates random datetime instance with lower/upper bounds.
        /// </summary>
        /// <param name="lowYear"></param>
        /// <param name="highYear"></param>
        /// <returns></returns>
        public DateTime GenererateRandomDate(int lowYear, int highYear)
        {
            //var random = new Random();
            int year = generator.Next(lowYear, highYear);
            int month = generator.Next(1, 12);
            int day = DateTime.DaysInMonth(year, month);
            int Day = generator.Next(1, day);
            DateTime dt = new DateTime(year, month, Day, 8, 00, 00);
            return dt;
        }
    }
}

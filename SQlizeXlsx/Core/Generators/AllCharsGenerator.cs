﻿using SQlizeXlsx.Core.Randomized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Generator for general pursposes, e.g. nicknames, dummy passwords, etc.
    /// </summary>
    public class AllCharsGenerator : IRandomType
    {
        private static char[] allchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-".ToCharArray();
        public char[] alphabet()
        {
            return allchars;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Numeric generator with lower/upper bounds.
    /// </summary>
    public class NumberGenerator
    {
        private Random random = new Random();

        public int NextInteger(int low, int high)
        {
            return random.Next(low, high);
        }

        public string NextDouble(int lowerBound, int upperBound, int decimalPlaces)
        {
            if (upperBound <= lowerBound || decimalPlaces < 0)
            {
                throw new ArgumentException("Error occured");
            }
            double value = ((random == null ? new Random() : random).NextDouble() * (upperBound - lowerBound)) + lowerBound;
            string formatted = value.ToString("N7", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            return formatted;
        }
    }
}

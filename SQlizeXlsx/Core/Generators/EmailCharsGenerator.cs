﻿using SQlizeXlsx.Core.Randomized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Generator for email names.
    /// </summary>
    public class EmailCharsGenerator : IRandomType
    {
        private static char[] emailChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-".ToCharArray();
        public virtual char[] alphabet()
        {
            return emailChars;
        }
    }
}

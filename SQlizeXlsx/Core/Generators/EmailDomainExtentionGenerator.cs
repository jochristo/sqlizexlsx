﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Generator for email domain extentions.
    /// </summary>
    public class EmailDomainExtentionGenerator : EmailCharsGenerator
    {
        private static char[] chars = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        public override char[] alphabet()
        {
            return chars;
        }
    }
}

﻿using SQlizeXlsx.Core.Randomized;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Core.Generators
{
    /// <summary>
    /// Generates random sequences of characters defined in character alphabets of type T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CharsGenerator<T> where T : IRandomType, new()
    {
        private IRandomType iRandomType = new T();
        public string GetRandomChars(int maxSize)
        {
            var chars = iRandomType.alphabet(); // get alphabet to use from type T.
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            var random = new Random();
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
                if (result.Length == 1)
                {
                    while (result[0] == '0')
                    {
                        result[0] = random.Next(1, 9).ToString().ToCharArray().ElementAt(0);
                    }
                }
            }
            return result.ToString();
        }
    }
}

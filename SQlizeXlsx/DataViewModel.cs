﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SQlizeXlsx.Mapping
{
    /// <summary>
    /// Represents the ViewModel class for the main Application page.
    /// </summary>
    public partial class DataViewModel : INotifyPropertyChanged
    {
        private string _profilesFilename = string.Empty;
        private string _categoriesFilename = string.Empty;
        private string _phrasesFilename = string.Empty;
        private string _answersFilename = string.Empty;
        private string _phraseCategoriesFilename = string.Empty;
        private string _dbname = string.Empty;
        private bool _isProfilesMode = false;
        private bool _isCategoriesMode = false;
        private bool _isPhrasesMode = false;
        private bool _isAnswersMode = false;
        private bool _isPhraseCategoriesMode = false;
        private string _script = string.Empty;
        private string _message = string.Empty;
        private bool _isGenerationEnabled = false;
        private bool _isFileGenerationEnabled = false;
        private bool _isCleared = false;
        private System.Windows.Visibility _clearAllVisibility = System.Windows.Visibility.Collapsed;
        private bool _isProfilesChecked = false;
        private bool _isPhraseCategoriesChecked = false;

        public string ProfilesFilename
        {
            get
            {
                return _profilesFilename;
            }

            set
            {
                _profilesFilename = value;
                OnPropertyChanged("ProfilesFilename");
            }
        }

        public string CategoriesFilename
        {
            get
            {
                return _categoriesFilename;
            }

            set
            {
                _categoriesFilename = value;
                OnPropertyChanged("CategoriesFilename");
            }
        }

        public string PhrasesFilename
        {
            get
            {
                return _phrasesFilename;
            }

            set
            {
                _phrasesFilename = value;
                OnPropertyChanged("PhrasesFilename");
            }
        }

        public string AnswersFilename
        {
            get
            {
                return _answersFilename;
            }

            set
            {
                _answersFilename = value;
                OnPropertyChanged("AnswersFilename");
            }
        }

        public string PhraseCategoriesFilename
        {
            get
            {
                return _phraseCategoriesFilename;
            }

            set
            {
                _phraseCategoriesFilename = value;
                OnPropertyChanged("PhraseCategoriesFilename");
            }
        }

        public string Dbname
        {
            get
            {
                return _dbname;
            }

            set
            {
                _dbname = value;
                OnPropertyChanged("Dbname");
                if ((!String.IsNullOrEmpty(Dbname) && !String.IsNullOrWhiteSpace(Dbname)) && (IsProfilesMode || IsCategoriesMode || IsPhrasesMode || IsAnswersMode || IsPhraseCategoriesMode))
                {
                    this.IsGenerationEnabled = true;
                }
                else
                {
                    this.IsGenerationEnabled = false;
                }
            }
        }

        public bool IsProfilesMode
        {
            get
            {
                return _isProfilesMode;
            }

            set
            {
                _isProfilesMode = value;
                UpdateFilenamesOnModeChanged(DataFileMode.Profiles, value);
                OnPropertyChanged("IsProfilesMode");
                if (value == true)// && !String.IsNullOrWhiteSpace(Dbname) && !String.IsNullOrEmpty(Dbname))
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public bool IsCategoriesMode
        {
            get
            {
                return _isCategoriesMode;
            }

            set
            {
                _isCategoriesMode = value;
                UpdateFilenamesOnModeChanged(DataFileMode.Categories, value);
                OnPropertyChanged("IsCategoriesMode");

                if(value == true)// && !String.IsNullOrWhiteSpace(Dbname) && !String.IsNullOrEmpty(Dbname))
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public bool IsPhrasesMode
        {
            get
            {
                return _isPhrasesMode;
            }

            set
            {
                _isPhrasesMode = value;
                UpdateFilenamesOnModeChanged(DataFileMode.Phrases, value);
                OnPropertyChanged("IsPhrasesMode");
                if (value == true)// && !String.IsNullOrWhiteSpace(Dbname) && !String.IsNullOrEmpty(Dbname))
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public bool IsAnswersMode
        {
            get
            {
                return _isAnswersMode;
            }

            set
            {
                _isAnswersMode = value;
                UpdateFilenamesOnModeChanged(DataFileMode.Answers, value);
                OnPropertyChanged("IsAnswersMode");
                if (value == true) // && !String.IsNullOrWhiteSpace(Dbname) && !String.IsNullOrEmpty(Dbname))
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public bool IsPhraseCategoriesMode
        {
            get
            {
                return _isPhraseCategoriesMode;
            }

            set
            {
                _isPhraseCategoriesMode = value;
                UpdateFilenamesOnModeChanged(DataFileMode.PhraseCategories, value);
                OnPropertyChanged("IsPhraseCategoriesMode");
                if (value == true) //&& !String.IsNullOrWhiteSpace(Dbname) && !String.IsNullOrEmpty(Dbname))
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public string Script
        {
            get
            {
                return _script;
            }

            set
            {
                _script = value;
                OnPropertyChanged("Script");
            }
        }

        public bool IsGenerationEnabled
        {
            get
            {
                return _isGenerationEnabled;
            }

            set
            {
                _isGenerationEnabled = value;
                OnPropertyChanged("IsGenerationEnabled");
            }
        }

        public bool IsCleared
        {
            get
            {
                return _isCleared;
            }

            set
            {
                _isCleared = value;
                OnPropertyChanged("IsCleared");
                if(value == true)
                {
                    ClearBindings();
                }
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        public bool IsFileGenerationEnabled
        {
            get
            {
                return _isFileGenerationEnabled;
            }

            set
            {
                _isFileGenerationEnabled = value;
                OnPropertyChanged("IsFileGenerationEnabled");                
            }
        }

        public Visibility ClearAllVisibility
        {
            get
            {
                return _clearAllVisibility;
            }

            set
            {
                _clearAllVisibility = value;
                OnPropertyChanged("ClearAllVisibility");
            }
        }

        public bool IsProfilesChecked
        {
            get
            {
                return _isProfilesChecked;
            }

            set
            {
                _isProfilesChecked = value;
                OnPropertyChanged("IsProfilesChecked");
                if (value == true)
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }

        public bool IsPhraseCategoriesChecked
        {
            get
            {
                return _isPhraseCategoriesChecked;
            }

            set
            {
                _isPhraseCategoriesChecked = value;
                OnPropertyChanged("IsPhraseCategoriesChecked");
                if (value == true)
                {
                    this.IsGenerationEnabled = true;
                }
            }
        }
        
        // Declare the event
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Updates data filename for given filemode.
        /// /// Resets to empty string if false.
        /// </summary>
        /// <param name="dataFileMode"></param>
        /// <param name="result"></param>
        private void UpdateFilenamesOnModeChanged(DataFileMode dataFileMode, bool result)
        {
            switch(dataFileMode)
            {
                case DataFileMode.Profiles:                    
                    if(result == false)
                    {
                        this.ProfilesFilename = string.Empty;
                    }
                    break;
                case DataFileMode.Categories:
                    if (result == false)
                    {
                        this.CategoriesFilename = string.Empty;
                    }
                    break;
                case DataFileMode.Phrases:
                    if (result == false)
                    {
                        this.PhrasesFilename = string.Empty;
                    }
                    break;
                case DataFileMode.Answers:
                    if (result == false)
                    {
                        this.AnswersFilename = string.Empty;
                    }
                    break;
                case DataFileMode.PhraseCategories:
                    if (result == false)
                    {
                        this.PhraseCategoriesFilename = string.Empty;
                    }
                    break;

            }
        }

        /// <summary>
        /// Resets all property bindings to their initial and default values.
        /// </summary>
        private void ClearBindings()
        {
            this.IsProfilesMode = false;
            this.IsCategoriesMode = false;
            this.IsPhrasesMode = false;
            this.IsAnswersMode = false;
            this.IsPhraseCategoriesMode = false;
            UpdateFilenamesOnModeChanged(DataFileMode.Profiles, false);
            UpdateFilenamesOnModeChanged(DataFileMode.Categories, false);
            UpdateFilenamesOnModeChanged(DataFileMode.Phrases, false);
            UpdateFilenamesOnModeChanged(DataFileMode.PhraseCategories, false);
            UpdateFilenamesOnModeChanged(DataFileMode.Answers, false);
            this.Script = string.Empty;
            this.Dbname = string.Empty;
            this.Message = string.Empty;
            this.IsGenerationEnabled = false;            
            this.IsFileGenerationEnabled = false;
            this.ClearAllVisibility = Visibility.Collapsed;
            this.IsProfilesChecked = false;
            this.IsPhraseCategoriesChecked = false;
        }
    }

    /// <summary>
    /// Supports data file mode.
    /// </summary>
    public enum DataFileMode
    {        
        Profiles, Categories, Phrases, Answers, PhraseCategories
    }


}

﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using static SQlizeXlsx.Linq2Excel.Validation;
using static SQlizeXlsx.Linq2Excel.SQLize;
using SQlizeXlsx.Mapping;
using SQlizeXlsx.Core;
using SQlizeXlsx.Core.Collections;
using System.Windows.Controls.Primitives;
using static SQlizeXlsx.Core.Exceptions.AppExceptions;
using SQlizeXlsx.Core.Domain;
using SQlizeXlsx.Domain;
using SQlizeXlsx.Core.Generators;

namespace SQlizeXlsx
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string COMPLETE_MYSQL_STATEMENT = "";
        private bool OkToGenerateSqlFile = false;
        private string finalSqlString = "";
        private string currentSqlScript = "";
        private string currentSqlScriptPrefix = "";
        private CellType currentCellType = CellType.STRING;

        //this page view model object
        private DataViewModel dataViewModel;

        public MainWindow()
        {
            InitializeComponent();
            this.SqlScriptButton.IsEnabled = false;

            // Set this page's datacontext - Attach all bindings
            dataViewModel = new DataViewModel();
            DataContext = dataViewModel;
            AttachBindings();
        }

        /// <summary>
        /// // Attach all bindings to input elements
        /// </summary>
        private void AttachBindings()
        {
            // Attach all bindings to input elements
            profileFilename.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("ProfilesFilename"),
                NotifyOnTargetUpdated = true
            });
            categoriesFileName.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("CategoriesFilename"),
                NotifyOnTargetUpdated = true
            });
            phrasesFileName.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("PhrasesFilename"),
                NotifyOnTargetUpdated = true
            });
            answersFileName.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("AnswersFilename"),
                NotifyOnTargetUpdated = true
            });
            this.phraseCategoryFileName.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("PhraseCategoriesFilename"),
                NotifyOnTargetUpdated = true
            });
            dbname.SetBinding(TextBox.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("Dbname")
            });
            this.SuccessFile.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("Message")
            });
            SqlScript.SetBinding(TextBox.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("Script")
            });
            SqlScriptButton.SetBinding(Button.IsEnabledProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsGenerationEnabled")
            });
            ClearButton.SetBinding(Button.VisibilityProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("ClearAllVisibility")
            });
            SqlFileButton.SetBinding(Button.IsEnabledProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsFileGenerationEnabled")
            });
                        
            toggleProfiles.SetBinding(ToggleButton.IsCheckedProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsProfilesChecked")
            });
            togglePhraseCategories.SetBinding(ToggleButton.IsCheckedProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsPhraseCategoriesChecked")
            });

            ProfilesSizeSlider.SetBinding(Slider.IsEnabledProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsProfilesChecked")
            });
        }
        
        /// <summary>
        /// Checks if the file extension is in the correct format.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool SqlizeXlsX(string filename)
        {
            string extension = Path.GetExtension(filename);
            if (extension == ".xlsx" || extension == ".XLSX" || extension == ".xls" || extension == ".XLS")
            {
                SqlScript.Text = "";
                this.SqlScript.Text = "";
                return true;
            }
            else
            {
                throw new WrongFileFormatException("Λάθος τύπος αρχείου ( μόνον XLSX)");
            }
        }           

        #region WPF BUTTON CLICK EVENTS

        /// <summary>
        /// Handles the click event to generated T-SQL insert script asynchronously.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenerateScriptAsync_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(async () =>
            {
                await this.Dispatcher.Invoke(async () =>
                {
                    this.progressBar.Visibility = Visibility.Visible;                    
                    string x = await GenerateScriptAsync();
                    this.progressBar.Visibility = Visibility.Collapsed;
                }, DispatcherPriority.Normal);
            });
        }
        
        /// <summary>
        /// Event to handle SQL file button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SqlFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (OkToGenerateSqlFile)
            {
                await WriteSQLFileAsync(COMPLETE_MYSQL_STATEMENT);
            }
        }

        /// <summary>
        /// Clears viewmodels IsCleared property  value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            this.dataViewModel.IsCleared = true;
        }

        #endregion

        #region GENERATE T-SQL SCRIPT ASYNC
        
        /// <summary>
        /// Creates the SQL text script and populates text field with data.
        /// </summary>
        /// <returns></returns>
        private async Task<string> GenerateScriptAsync()
        {
            // do empty values checking
            try
            {
                //clear complete sql script
                COMPLETE_MYSQL_STATEMENT = string.Empty;

                OkToGenerateSqlFile = false;
                SqlFileButton.Visibility = Visibility.Collapsed;
                SuccessFile.Visibility = Visibility.Visible;
                SuccessFile.Text = "Reading files...";

                // include dbname
                if (!dataViewModel.Dbname.IsNullOrEmptyOrWhiteSpace())
                {
                    COMPLETE_MYSQL_STATEMENT += "USE " + dataViewModel.Dbname + ";\n\n";
                }

                // Get xlsx data
                bool isValid = true;
                var invalidCount = 0;
                var error = string.Empty;
                var relationalChecker = new ReferentialIntegrityValidation(); // referencial integrity checks

                // Copy to cache to use later as foreign key reference in answers collection
                IEnumerable<ProfileDTO> profilesCache = new List<ProfileDTO>();
                IEnumerable<PhraseDTO> phrasesCache = new List<PhraseDTO>();
                IEnumerable<CategoryDTO> categoriesCache = new List<CategoryDTO>();

                if (dataViewModel.IsProfilesMode)
                {
                    dataViewModel.Message = "Reading profiles data...";
                    var collection = new ProfileCollection(Convert.ToInt32(this.ProfilesSizeSlider.Value));
                    var data = await collection.GetDataAsync();                                        
                    isValid = data.IsValid;
                    invalidCount = !isValid ? invalidCount + 1 : invalidCount;
                    if (!isValid)
                    {
                        error += "\n" + data.Value;
                    }
                    else
                    {
                        COMPLETE_MYSQL_STATEMENT += "\n" + data;
                        relationalChecker.Profiles = data.Data;

                        // Copy to cache
                        profilesCache = data.Data.AsEnumerable();
                    }
                }

                if (dataViewModel.IsCategoriesMode)
                {
                    dataViewModel.Message = "Reading categories data...";
                    var collection = new CategoryCollection(categoriesFileName.Text); // Store phrases read from excel file
                    var data = await collection.GetDataAsync(); // Get data async                                             
                    isValid = data.IsValid;
                    invalidCount = !isValid ? invalidCount + 1 : invalidCount;
                    if (!isValid)
                    {
                        error += "\n" + data.Value;
                    }
                    else
                    {
                        COMPLETE_MYSQL_STATEMENT += "\n" + data;
                        relationalChecker.Categories = data.Data;
                        // Copy to cache
                        categoriesCache = data.Data.AsEnumerable();
                    }
                }

                if (dataViewModel.IsPhrasesMode)
                {
                    dataViewModel.Message = "Reading phrases data...";                    
                    var collection = new PhraseCollection(phrasesFileName.Text); // store phrases read from excel file
                    var data = await collection.GetDataAsync(); // get data async
                    isValid = data.IsValid;
                    invalidCount = !isValid ? invalidCount + 1 : invalidCount;
                    if (!isValid)
                    {
                        error += "\n" + data.Value;
                    }
                    else
                    {
                        COMPLETE_MYSQL_STATEMENT += "\n" + data;
                        relationalChecker.Phrases = data.Data;
                        // Copy to cache
                        phrasesCache = data.Data.AsEnumerable();
                    }
                }

                if (dataViewModel.IsPhraseCategoriesMode)
                {

                    if (!dataViewModel.IsCategoriesMode && !dataViewModel.IsPhraseCategoriesMode)
                    {
                        throw new ArgumentException("No category and phrase data has been selected.");
                    }

                    dataViewModel.Message = "Reading phrase-categories data...";                    
                    var collection = new PhraseCategoryCollection(phrasesCache, categoriesCache); // Get data from collections cache
                    var data = await collection.GetDataAsync(); // get data async                                                        
                    isValid = data.IsValid;
                    invalidCount = !isValid ? invalidCount + 1 : invalidCount;
                    if (!isValid)
                    {
                        error += "\n" + data.Value;
                    }
                    else
                    {
                        COMPLETE_MYSQL_STATEMENT += "\n" + data;
                        relationalChecker.PhraseCategories = data.Data;
                    }
                }
                if (dataViewModel.IsAnswersMode)
                {

                    if (!dataViewModel.IsProfilesMode && !dataViewModel.IsPhrasesMode)
                    {
                        throw new ArgumentException("No profile and phrase data has been selected.");
                    }

                    dataViewModel.Message = "Reading answers data...";
                    var collection = new AnswerCollection(answersFileName.Text); // store phrases read from excel file
                    var data = await collection.GetDataAsync(); // get data async                    
                    isValid = data.IsValid;
                    invalidCount = !isValid ? invalidCount + 1 : invalidCount;
                    if (!isValid)
                    {
                        error += "\n" + data.Value;
                    }
                    else
                    {
                        // randomize profile id foreign key from profiles cache
                        var generator = new NumberGenerator();
                        var count = profilesCache.Count();

                        // NEED TO CHECK FOR DUPLICATE PAIRS OF an_phrase_id AND an_profile_id                        
                        var set = new HashSet<PhraseProfilePair>();
                        var map = new Dictionary<string, HashSet<string>>(); // phraseid- List of profile id pairs
                        var profiles = new HashSet<string>();                        
                        if (count > 1)
                        {
                            data.Data.ToList().ForEach(a =>
                            {
                                a.an_profile_id = generator.NextInteger(1, count+1).ToString();
                                if (!map.ContainsKey(a.an_phrase_id))
                                {
                                    profiles = new HashSet<string>();
                                    profiles.Add(a.an_profile_id);
                                    map.Add(a.an_phrase_id, profiles);
                                }
                                else
                                {
                                    var add = map[a.an_phrase_id].Add(a.an_profile_id);
                                    while (!add)
                                    {
                                        if (map[a.an_phrase_id].Count == count)
                                        {
                                            throw new DuplicateDataException("Answers data file: No more profile id to pair with phrase id. Consider generating more profiles.");
                                        }
                                        a.an_profile_id = generator.NextInteger(1, count+1).ToString();
                                        add = map[a.an_phrase_id].Add(a.an_profile_id);
                                    }
                                }
                            });
                            data.Value = JoinSqlInserts(data.Data);
                            COMPLETE_MYSQL_STATEMENT += "\n" + data.Value;
                        }
                        else if (count <= 1)
                        {
                            COMPLETE_MYSQL_STATEMENT += "\n" + data;
                        }

                        relationalChecker.Answers = data.Data;
                    }
                }

                // FORMAT VALIDATION ERRORS FOUND
                if (invalidCount > 0)
                {
                    throw new InvalidFormatException(error);
                }

                //referential integrity check
                if (!relationalChecker.Validate())
                {
                    var header = "[Referential integrity errors] :\n";
                    throw new InvalidFormatException(header + relationalChecker.Results.Messages());
                }

                this.SqlScript.Text = COMPLETE_MYSQL_STATEMENT;
                if (SqlScript.Text != "")
                {
                    this.SuccessFile.Text = "Done.";
                    OkToGenerateSqlFile = true;
                    this.SqlFileButton.Visibility = Visibility.Visible;
                    this.SqlScriptButton.Visibility = Visibility.Collapsed;
                    this.dataViewModel.IsFileGenerationEnabled = true;
                    return COMPLETE_MYSQL_STATEMENT;
                }
                else
                {
                    this.SuccessFile.Text = "Failed.";
                }
            }
            catch (Exception ex)
            {
                if (ex is InvalidFormatException)
                {
                    MessageBox.Show("Error parsing excel files", "Μήνυμα");
                    this.SuccessFile.Text = "Failed!";
                    this.dataViewModel.Script = ex.Message.ToString();
                    this.dataViewModel.IsGenerationEnabled = false;
                }
                else
                {
                    var message = ex.Message.ToString();
                    MessageBox.Show(message, "Μήνυμα");
                    this.SuccessFile.Text = "Failed!";
                    this.dataViewModel.Script = message;
                }
            }
            return null;
        }

        /// <summary>
        /// Write SQL script into file.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public async Task WriteSQLFileAsync(string text)
        {

            this.SuccessFile.Visibility = Visibility.Visible;
            this.SuccessFile.Text = "Generating file...";
            // Set a variable to the My Documents path.
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Write the text asynchronously to a new file named "WriteTextAsync.txt".
            var fileTypeName = "SQL_script";
            using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\" + fileTypeName + ".sql"))
            {
                try
                {
                    await outputFile.WriteAsync(text);
                    this.SuccessFile.Text = "SQl script created.";
                    MessageBox.Show("Το αρχείο δημιουργήθηκε, κάνε τη δουλειά σου γρήγορα!", "SQL script - Success");
                    //this.SuccessFile.Visibility = Visibility.Hidden;
                    System.Diagnostics.Process.Start(mydocpath + @"\" + fileTypeName + ".sql");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Κάτι πήγε στραβά!", "Exception Message Box");
                    this.SuccessFile.Visibility = Visibility.Hidden;
                }

            }
        }

        #endregion

        #region DATAFILES BUTTON EVENTS

        private void ProfilesDataButton_Click(object sender, RoutedEventArgs e)
        {
            this.SqlScriptButton.Content = "Generate SQL";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    SqlizeXlsX(openFileDialog.FileName);
                    this.profileFilename.Text = openFileDialog.FileName;
                    this.dataViewModel.ProfilesFilename = openFileDialog.FileName;
                    //set which  sql script/prefix  to use
                    currentSqlScript = Linq2Excel.SqlScript.PROFILE_INSERT_SQL;
                    currentSqlScriptPrefix = Linq2Excel.SqlScript.PROFILE_INSERT_SQL_PREFIX;
                    this.SqlFileButton.Visibility = Visibility.Collapsed;
                    this.SqlScriptButton.Visibility = Visibility.Visible;
                    this.ClearButton.Visibility = Visibility.Visible;
                    SuccessFile.Text = "";
                }
                catch (WrongFileFormatException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Μήνυμα");
                }
            }
        }

        private void CategoriesDataButton_Click(object sender, RoutedEventArgs e)
        {
            this.SqlScriptButton.Content = "Generate SQL";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    SqlizeXlsX(openFileDialog.FileName);
                    this.categoriesFileName.Text = openFileDialog.FileName;
                    this.dataViewModel.CategoriesFilename = openFileDialog.FileName;
                    //set which  sql script/prefix  to use
                    currentSqlScript = Linq2Excel.SqlScript.CATEGORY_INSERT_SQL;
                    currentSqlScriptPrefix = Linq2Excel.SqlScript.CATEGORY_INSERT_SQL_PREFIX;
                    this.SqlFileButton.Visibility = Visibility.Collapsed;
                    this.SqlScriptButton.Visibility = Visibility.Visible;
                    this.ClearButton.Visibility = Visibility.Visible;
                    SuccessFile.Text = "";
                }
                catch (WrongFileFormatException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Μήνυμα");
                }
            }
        }

        private void AnswersDataButton_Click(object sender, RoutedEventArgs e)
        {
            this.SqlScriptButton.Content = "Generate SQL";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    SqlizeXlsX(openFileDialog.FileName);
                    this.dataViewModel.AnswersFilename = openFileDialog.FileName;
                    //set which  sql script/prefix  to use
                    currentSqlScript = Linq2Excel.SqlScript.ANSWER_INSERT_SQL;
                    currentSqlScriptPrefix = Linq2Excel.SqlScript.ANSWER_INSERT_SQL_PREFIX;
                    this.SqlFileButton.Visibility = Visibility.Collapsed;
                    this.SqlScriptButton.Visibility = Visibility.Visible;
                    this.ClearButton.Visibility = Visibility.Visible;
                    SuccessFile.Text = "";
                }
                catch (WrongFileFormatException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Μήνυμα");
                }
            }
        }

        private void PhrasesDataButton_Click(object sender, RoutedEventArgs e)
        {
            this.SqlScriptButton.Content = "Generate SQL";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    SqlizeXlsX(openFileDialog.FileName);
                    this.dataViewModel.PhrasesFilename = openFileDialog.FileName;
                    //set which  sql script/prefix  to use
                    currentSqlScript = Linq2Excel.SqlScript.PHRASE_INSERT_SQL;
                    currentSqlScriptPrefix = Linq2Excel.SqlScript.PHRASE_INSERT_SQL_PREFIX;
                    this.SqlFileButton.Visibility = Visibility.Collapsed;
                    this.SqlScriptButton.Visibility = Visibility.Visible;
                    this.ClearButton.Visibility = Visibility.Visible;
                    SuccessFile.Text = "";
                }
                catch (WrongFileFormatException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Μήνυμα");
                }
            }
        }

        private void PhraseCategoryDataButton_Click(object sender, RoutedEventArgs e)
        {
            this.SqlScriptButton.Content = "Generate SQL";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    SqlizeXlsX(openFileDialog.FileName);
                    this.dataViewModel.PhraseCategoriesFilename = openFileDialog.FileName;
                    //set which  sql script/prefix  to use
                    currentSqlScript = Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL;
                    currentSqlScriptPrefix = Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL_PREFIX;
                    this.SqlFileButton.Visibility = Visibility.Collapsed;
                    this.SqlScriptButton.Visibility = Visibility.Visible;
                    this.ClearButton.Visibility = Visibility.Visible;
                    SuccessFile.Text = "";
                }
                catch (WrongFileFormatException ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Μήνυμα");
                }
            }
        }

        #endregion DATAFILES BUTTON EVENTS

        #region TargetUpdated , Changed EVENTS

        /// <summary>
        /// Db name text box changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dbname_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            // update viewmodel binding properties           
            dataViewModel.Dbname = textBox.Text;
        }

        private void categoriesFileName_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            TextBlock tb = e.OriginalSource as TextBlock;
            if (!string.IsNullOrEmpty(tb.Text) && !string.IsNullOrWhiteSpace(tb.Text))
            {
                this.dataViewModel.IsCategoriesMode = true;
                this.dataViewModel.IsCleared = false;
            }
        }

        private void answersFileName_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            TextBlock tb = e.OriginalSource as TextBlock;
            if (!string.IsNullOrEmpty(tb.Text) && !string.IsNullOrWhiteSpace(tb.Text))
            {
                this.dataViewModel.IsAnswersMode = true;
                this.dataViewModel.IsCleared = false;
            }
        }

        private void profileFilename_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            TextBlock tb = e.OriginalSource as TextBlock;
            if (!string.IsNullOrEmpty(tb.Text) && !string.IsNullOrWhiteSpace(tb.Text))
            {
                this.dataViewModel.IsProfilesMode = true;
                this.dataViewModel.IsCleared = false;
            }
        }

        private void phrasesFileName_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            TextBlock tb = e.OriginalSource as TextBlock;
            if (!string.IsNullOrEmpty(tb.Text) && !string.IsNullOrWhiteSpace(tb.Text))
            {
                this.dataViewModel.IsPhrasesMode = true;
                this.dataViewModel.IsCleared = false;
            }
        }

        private void phraseCategoryFileName_TargetUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            TextBlock tb = e.OriginalSource as TextBlock;
            if (!string.IsNullOrEmpty(tb.Text) && !string.IsNullOrWhiteSpace(tb.Text))
            {
                this.dataViewModel.IsPhraseCategoriesMode = true;
                this.dataViewModel.IsCleared = false;
            }
        }

        private void ProfilesSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (sender as Slider).Value = Math.Round(e.NewValue, 0);
        }

        #endregion

        #region TOGGLE BUTTONS CHECKED EVENTS

        private void toggleProfiles_Checked(object sender, RoutedEventArgs e)
        {
            this.dataViewModel.IsProfilesMode = true;
            this.dataViewModel.IsCleared = false;

            //set which  sql script/prefix  to use
            currentSqlScript = Linq2Excel.SqlScript.PROFILE_INSERT_SQL;
            currentSqlScriptPrefix = Linq2Excel.SqlScript.PROFILE_INSERT_SQL_PREFIX;
            this.SqlFileButton.Visibility = Visibility.Collapsed;
            this.SqlScriptButton.Visibility = Visibility.Visible;
            this.ClearButton.Visibility = Visibility.Visible;
            SuccessFile.Text = "";
        }

        private void toggleProfiles_Unchecked(object sender, RoutedEventArgs e)
        {
            this.dataViewModel.IsProfilesMode = false;            
        }

        private void togglePhraseCategories_Checked(object sender, RoutedEventArgs e)
        {
            this.dataViewModel.IsPhraseCategoriesMode = true;
            this.dataViewModel.IsCleared = false;

            //set which  sql script/prefix  to use
            currentSqlScript = Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL;
            currentSqlScriptPrefix = Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL_PREFIX;
            this.SqlFileButton.Visibility = Visibility.Collapsed;
            this.SqlScriptButton.Visibility = Visibility.Visible;
            this.ClearButton.Visibility = Visibility.Visible;
            SuccessFile.Text = "";
        }

        private void togglePhraseCategories_Unchecked(object sender, RoutedEventArgs e)
        {
            this.dataViewModel.IsPhraseCategoriesMode = false;
        }

        #endregion

        #region OLD OpenXML read methods

        /// <summary>
        /// Generates SQL script per data file given
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="tableNameType"></param>
        /// <returns></returns>
        private Task<string> GenerateSqlScriptPerDataFile(string filename, TableType tableNameType, out bool isSuccessful)
        {
            isSuccessful = false;
            return Task<string>.Run(() =>
            {
                try
                {
                    // SET SQL SCRIPT PREFIX BASED ON TABLENAME TYPE
                    string itemScript = string.Empty;
                    string completeScript = string.Empty;
                    int gapsCellPosition = 5;
                    int phraseCellPosition = 8;
                    int answerCellPosition = 2;
                    int phraseidFKCellPosition = 8;
                    var errors = new List<String>();
                    var phraseMap = new Dictionary<int, int>(); //phraseid - gaps pair
                    var answerMap = new Dictionary<int, string>(); // answerid - answers pair
                    var answerPhraseRefMap = new Dictionary<int, int>(); // answerid - phraseid pair

                    if (tableNameType == TableType.CATEGORIES)
                    {
                        currentSqlScriptPrefix = Linq2Excel.SqlScript.CATEGORY_INSERT_SQL_PREFIX;
                        itemScript += Linq2Excel.SqlScript.CATEGORY_INSERT_SQL;
                    }
                    else if (tableNameType == TableType.ANSWERS)
                    {
                        currentSqlScriptPrefix = Linq2Excel.SqlScript.ANSWER_INSERT_SQL_PREFIX;
                        itemScript += Linq2Excel.SqlScript.ANSWER_INSERT_SQL;
                    }
                    else if (tableNameType == TableType.PROFILES)
                    {
                        currentSqlScriptPrefix = Linq2Excel.SqlScript.PROFILE_INSERT_SQL_PREFIX;
                        itemScript += Linq2Excel.SqlScript.PROFILE_INSERT_SQL;
                    }
                    else if (tableNameType == TableType.PHRASES)
                    {
                        currentSqlScriptPrefix = Linq2Excel.SqlScript.PHRASE_INSERT_SQL_PREFIX;
                        itemScript += Linq2Excel.SqlScript.PHRASE_INSERT_SQL;
                    }
                    else if (tableNameType == TableType.PHRASE_CATEGORIES)
                    {
                        currentSqlScriptPrefix = Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL_PREFIX;
                        itemScript += Linq2Excel.SqlScript.PHRASE_CATEGORY_INSERT_SQL;
                    }

                    string fileName = filename;
                    Stream stream = File.Open(@fileName, FileMode.Open);
                    finalSqlString = string.Empty;

                    // Open a SpreadsheetDocument based on a package.
                    using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, false))
                    {
                        try
                        {
                            WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                            Sheet theSheet = workbookPart.Workbook.Descendants<Sheet>().First();
                            // Retrieve a reference to the worksheet part.
                            WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(theSheet.Id));
                            IEnumerable<Cell> theCells = worksheetPart.Worksheet.Descendants<Cell>();
                            IEnumerable<Row> rows = worksheetPart.Worksheet.Descendants<Row>();
                            var rowCount = rows.Count();
                            var list = theCells.ToList();
                            var value = "";
                            var cellCounter = 1;
                            bool isNull = false;
                            int currentCell = 1;
                            int totalCells = theCells.Count();
                            if (rowCount == 0)
                            {
                                throw new EmptyFileException("Βρέθηκε κενό excel!");
                            }
                            else
                            {
                                var rowCellCounter = theCells.Count() / rowCount;
                                var index = 0;
                                var currentRow = 1;
                                var currentRowCell = 1;
                                var gaps = 0;
                                var phraseid = 0;
                                var answerid = 0;

                                foreach (var c in theCells)
                                {
                                    //strings
                                    List<SharedStringItem> sharedStrings =
                                        workbookPart.SharedStringTablePart.SharedStringTable.ChildElements.OfType<SharedStringItem>().ToList();
                                    value = c.InnerText;
                                    int sharedStringIndex = 0;


                                    //empty cell
                                    if (c.InnerText == string.Empty)
                                    {
                                        //errors.Add("Empty cell found: at row " + currentRow + " cell " + cellCounter);                                        
                                        throw new InvalidFormatException("Empty cell(s) found. Use either Nil/Null in empty cells");
                                    }


                                    if (int.TryParse(c.InnerText, out sharedStringIndex) && sharedStrings.Count > sharedStringIndex)
                                    //&& sharedStrings[sharedStringIndex].Text != null)
                                    {
                                        value = sharedStrings[sharedStringIndex].Text.Text;
                                        currentCellType = CellType.STRING;
                                        if (value == "NULL" || value == "null")
                                        {
                                            value = "NULL";
                                            isNull = true;
                                            currentCellType = CellType.NULL;
                                        }
                                        else if (value == "nil" || value == "NIL")
                                        {
                                            value = "";
                                            isNull = false;
                                            currentCellType = CellType.EMPTY;
                                        }
                                    }

                                    // numbers
                                    if (c.DataType == null)
                                    {
                                        value = c.CellValue.InnerText;
                                        currentCellType = CellType.NUMERIC;
                                    }


                                    if (rowCellCounter == cellCounter)
                                    {
                                        // NUMERIC OR NULL CELL
                                        if (currentCellType == CellType.NULL || currentCellType == CellType.NUMERIC)
                                        {
                                            //check for gaps, phrase, answer cell                                            
                                            if (tableNameType == TableType.PHRASES) // PHRASES DATA
                                            {
                                                if (currentRow > 1 && cellCounter == gapsCellPosition)
                                                {
                                                    gaps = Int32.Parse(value);
                                                    //update gaps value in phrase map
                                                    if (phraseMap.ContainsKey(phraseid))
                                                    {
                                                        //phraseMap[phraseid] = gaps;
                                                    }
                                                }
                                                else if (currentRow > 1 && cellCounter == 1) //phrase_id cell
                                                {
                                                    //store phrase id in map
                                                    //phraseid = Int32.Parse(value);
                                                    //phraseMap.Add(Int32.Parse(value), 0); //temporarilly 0
                                                }
                                            }
                                            else if (tableNameType == TableType.ANSWERS) //ΑNSWERS DATA
                                            {
                                                if (currentRow > 1 && cellCounter == 1) //answerid cell position
                                                {
                                                    //answerid = Int32.Parse(value);
                                                    //answerMap.Add(Int32.Parse(value), ""); //temporarilly empty string
                                                }
                                                else if (currentRow > 1 && cellCounter == phraseidFKCellPosition)
                                                {
                                                    //validate against phrase 
                                                    //var phraseGaps = 0;
                                                    //var fkPhraseid = Int32.Parse(value);
                                                    //if(phraseMap.ContainsKey(fkPhraseid))
                                                    //{
                                                    // phraseGaps = phraseMap[fkPhraseid];
                                                    //check answer json string against given phrase and gaps
                                                    //var isValid = isAnswerValid(answerMap[answerid],phraseGaps);
                                                    //}
                                                }
                                            }

                                            itemScript += value;
                                        }
                                        // STRING-VALUED OR EMPTY CELL
                                        else if (currentCellType == CellType.STRING || currentCellType == CellType.EMPTY)
                                        {
                                            //check for char [']
                                            bool contains = value.Contains("'");
                                            String parsed = string.Empty;
                                            parsed = value;
                                            if (contains)
                                            {
                                                parsed = parsed.Replace("'", "\\'");
                                            }

                                            //check for gaps, phrase, answer cell                                            
                                            if (tableNameType == TableType.PHRASES)
                                            {
                                                //calculate phrase cell position in each cell and row iteration
                                                //var diffFromPhraseCell = (rowCellCounter * currentRow) - (phraseCellPosition * currentRow);
                                                //var diffFromGapsCell = (rowCellCounter * currentRow) - (gapsCellPosition * currentRow);
                                                if (currentRow > 1 && cellCounter == phraseCellPosition)
                                                {
                                                    if (!isPhraseValid(value, gaps))
                                                    {
                                                        //throw new Exception("Phrase text format is invalid: " + parsed);
                                                        errors.Add("Invalid phrase text format: <" + value + "> at row " + currentRow + " cell " + cellCounter);
                                                    }
                                                }
                                            }
                                            else if (tableNameType == TableType.ANSWERS)
                                            {
                                                if (currentRow > 1 && cellCounter == answerCellPosition)
                                                {
                                                    //try replace whitespace with one whitespace first and trim leading/trailing whitespace
                                                    var trimmed = value.Trim();
                                                    var replaced = Regex.Replace(trimmed, @"\s{2,}", " ");
                                                    if (!isJsonAnswerValid(replaced))
                                                    {
                                                        /*
                                                        var temp = "";
                                                        temp = result;
                                                        if (result == "valid")
                                                        {
                                                            temp = "Empty json answer: ";
                                                        }
                                                        */
                                                        //errors.Add("Invalid json answer format: <" + value + "> at row " + currentRow + " cell " + cellCounter);
                                                        errors.Add("Invalid json answer format: <" + value + "> at row " + currentRow + " cell " + cellCounter + " - ");
                                                    }
                                                    if (currentRow > 1 && cellCounter == answerCellPosition)
                                                    {
                                                        //update answer text value in map
                                                        if (answerMap.ContainsKey(answerid))
                                                        {
                                                            //answerMap[answerid] = parsed; 
                                                        }
                                                    }
                                                }
                                            }

                                            itemScript += "'" + parsed + "'"; //,'','','',','','','','','';                                            
                                        }
                                    }
                                    else if (cellCounter < rowCellCounter)
                                    {
                                        if (currentCellType == CellType.NULL || currentCellType == CellType.NUMERIC)
                                        {
                                            //check for gaps, phrase, answer cell                                            
                                            if (tableNameType == TableType.PHRASES) // PHRASES DATA
                                            {
                                                if (currentRow > 1 && cellCounter == gapsCellPosition)
                                                {
                                                    gaps = Int32.Parse(value);
                                                    //update gaps value in phrase map
                                                    if (phraseMap.ContainsKey(phraseid))
                                                    {
                                                        //phraseMap[phraseid] = gaps;
                                                    }
                                                }
                                                else if (currentRow > 1 && cellCounter == 1) //phrase_id cell
                                                {
                                                    //store phrase id in map
                                                    //phraseid = currentRow; // Int32.Parse(value);
                                                    //phraseMap.Add(currentRow, 0); //temporarilly 0
                                                }
                                            }
                                            else if (tableNameType == TableType.ANSWERS) //ΑNSWERS DATA
                                            {
                                                if (currentRow > 1 && cellCounter == 1) //answerid cell position
                                                {
                                                    //answerid = currentRow;
                                                    //answerMap.Add(currentRow, ""); //temporarilly empty string
                                                }
                                                else if (currentRow > 1 && cellCounter == phraseidFKCellPosition)
                                                {
                                                    //validate against phrase 
                                                    /*
                                                    var phraseGaps = 0;
                                                    var fkPhraseid = Int32.Parse(value);
                                                    if (phraseMap.ContainsKey(fkPhraseid))
                                                    {
                                                        phraseGaps = phraseMap[fkPhraseid];
                                                        //check answer json string against given phrase and gaps
                                                        var isValid = isAnswerValid(answerMap[answerid], phraseGaps);
                                                    }
                                                    */
                                                }
                                            }

                                            itemScript += value + ",";
                                        }
                                        else if (currentCellType == CellType.STRING || currentCellType == CellType.EMPTY)
                                        {
                                            //check for char [']
                                            bool contains = value.Contains("'");
                                            String parsed = string.Empty;
                                            parsed = value;
                                            if (contains)
                                            {
                                                parsed = parsed.Replace("'", "\\'");
                                            }

                                            itemScript += "'" + parsed + "',";

                                            //check for gaps, phrase, answer cell                                            
                                            if (tableNameType == TableType.PHRASES)
                                            {
                                                //calculate phrase cell position in each cell and row iteration
                                                //var diffFromPhraseCell = (rowCellCounter * currentRow) - (phraseCellPosition * currentRow);
                                                //var diffFromGapsCell = (rowCellCounter * currentRow) - (gapsCellPosition * currentRow);
                                                if (currentRow > 1 && cellCounter == phraseCellPosition)
                                                {
                                                    if (!isPhraseValid(value, gaps))
                                                    {
                                                        //throw new Exception("Invalid phrase text format is invalid: <" + parsed + "> at row "+currentRow +" cell "+ cellCounter);
                                                        errors.Add("Invalid phrase text format: <" + value + "> at row " + currentRow + " cell " + cellCounter);
                                                    }
                                                    int found = 0;
                                                    if (!isGapsValid(value, gaps, out found))
                                                    {
                                                        errors.Add("Inconsistent gaps : found " + found + " in phrase <" + value + "> given " + gaps + " at row " + currentRow + " cell " + cellCounter);
                                                    }
                                                }
                                            }
                                            else if (tableNameType == TableType.ANSWERS)
                                            {
                                                if (currentRow > 1 && cellCounter == answerCellPosition)
                                                {
                                                    //try replace whitespace with one whitespace first and trim leading/trailing whitespace
                                                    var trimmed = value.Trim();
                                                    var replaced = Regex.Replace(trimmed, @"\s{2,}", " ");
                                                    if (!isJsonAnswerValid(replaced))
                                                    {
                                                        /*
                                                        var temp = "";
                                                        temp = result;
                                                        if (result == "valid")
                                                        {
                                                            temp = "Empty json string";
                                                        }
                                                        */
                                                        errors.Add("Invalid json answer format: <" + value + "> at row " + currentRow + " cell " + cellCounter + " - ");
                                                        //errors.Add(temp + "<" + value + "> at row " + currentRow + " cell " + cellCounter);                                                        
                                                    }
                                                    if (currentRow > 1 && cellCounter == answerCellPosition)
                                                    {
                                                        //update answer text value in map
                                                        if (answerMap.ContainsKey(answerid))
                                                        {
                                                            //answerMap[answerid] = parsed;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (cellCounter == rowCellCounter)
                                    {
                                        //this.FilenameSelected.Text += ");\n";
                                        if (rowCount == currentCell)
                                        {
                                            itemScript += ");";
                                        }
                                        else
                                        {
                                            itemScript += ");\n";
                                        }
                                        cellCounter = 0;

                                        // DO NOT INCLUDE FIRST ROW CELL VALUES - HEADERS
                                        if (currentCell == rowCellCounter)
                                        {
                                            itemScript = currentSqlScriptPrefix;
                                        }
                                        else if (currentCell > rowCellCounter)
                                        {
                                            Console.Write(itemScript);
                                            completeScript += itemScript;
                                            itemScript = currentSqlScriptPrefix;
                                        }

                                    }
                                    currentCell++;
                                    cellCounter++;
                                    index++; //index counter

                                    //determine current row by comparing  current cell against total cells per row
                                    if (currentRowCell < rowCellCounter)
                                    {
                                        currentRowCell++;
                                    }
                                    else
                                    {
                                        currentRowCell = 1;
                                        currentRow++;
                                    }
                                } //end foreach

                                if (errors.Count > 0)
                                {
                                    String error = string.Empty;
                                    foreach (var c in errors)
                                    {
                                        error += c + "\n";
                                    }
                                    throw new InvalidFormatException(error);
                                }

                                return completeScript;
                            }
                        }
                        catch (Exception e)
                        {
                            if (e is InvalidFormatException)
                            {
                                throw new InvalidFormatException(e.Message.ToString());
                            }
                            throw new Exception(e.Message.ToString());
                        }
                    }
                }
                catch (FileFormatException ffe)
                {

                    throw new Exception("Λάθος τύπος αρχείου (μόνον XLSX)");
                }
            });
        }

        #endregion

    }

    /// <summary>
    /// Represents a JSON-formatted answer string.
    /// </summary>
    public class AnswerJson
    {
        /// <summary>
        /// Word field of json answer.
        /// </summary>
        public string word;
    }

    /// <summary>
    /// Represents the validation result of a JSON-formatted string.
    /// </summary>
    public enum JsonValidationResult
    {
        IsJson,     // json
        NotJson,    // invalid json
        EmptyJson   // empty json
    }

    public enum TableType
    {
        auth_user,
        auth_userprofile,
        CATEGORIES,
        PROFILES,
        PHRASES,
        ANSWERS,
        PHRASE_CATEGORIES
    }

    public enum CellType
    {
        STRING,
        NUMERIC,
        NULL,
        EMPTY
    }
}

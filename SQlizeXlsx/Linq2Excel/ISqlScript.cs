﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Represents an object mapped to excel row with specified SQL insert prefix.
    /// </summary>
    public interface ISqlScript
    {
        // Returns the insert prefix for the ISqlScript object.
        string InsertPrefix();
    }
}

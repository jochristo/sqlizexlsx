﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Provides static T-SQL insert statement prefix strings to use in domain's mappable objects insert operations.
    /// </summary>
    public static class SqlScript
    {
        private const string _profilesTablename = "user_profile";
        private const string _categoriesTablename = "category";        
        private const string _phrasesTablename = "phrase";
        private const string _answersTablename = "answer";
        private const string _phraseCategoryTablename = "phrase_category";

        public static string PROFILE_INSERT_SQL = "INSERT INTO " + _profilesTablename + " VALUES (";
        public static string PROFILE_INSERT_SQL_PREFIX = "INSERT INTO " + _profilesTablename + " VALUES (";
        public static string CATEGORY_INSERT_SQL = "INSERT INTO " + _categoriesTablename + " VALUES (";
        public static string CATEGORY_INSERT_SQL_PREFIX = "INSERT INTO " + _categoriesTablename + " VALUES (";
        public static string PHRASE_INSERT_SQL = "INSERT INTO " + _phrasesTablename + " VALUES (";
        public static string PHRASE_INSERT_SQL_PREFIX = "INSERT INTO " + _phrasesTablename + " VALUES (";
        public static string ANSWER_INSERT_SQL = "INSERT INTO " + _answersTablename + " VALUES (";
        public static string ANSWER_INSERT_SQL_PREFIX = "INSERT INTO " + _answersTablename + " VALUES (";
        public static string PHRASE_CATEGORY_INSERT_SQL = "INSERT INTO " + _phraseCategoryTablename + " VALUES (";
        public static string PHRASE_CATEGORY_INSERT_SQL_PREFIX = "INSERT INTO " + _phraseCategoryTablename + " VALUES (";
    }
}

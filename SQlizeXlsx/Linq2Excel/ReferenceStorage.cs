﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Class to store values and pass an instance of this class by value to the async method.
    /// </summary>
    /// <typeparam name="T">One object to keep</typeparam>
    /// <typeparam name="D">Second object to keep</typeparam>
    public class ReferenceStorage<T, D>
    {
        public ReferenceStorage() { }
        public ReferenceStorage(T value) { Value = value; }
        public ReferenceStorage(T value, IEnumerable<D> data) { Value = value; Data = data; }
        public T Value { get; set; }
        public IEnumerable<D> Data { get; set; }
        public bool IsValid { get; set; }
        public override string ToString()
        {
            T value = Value;
            return value == null ? "" : value.ToString();
        }
        public static implicit operator T(ReferenceStorage<T, D> r) { return r.Value; }
        public static implicit operator ReferenceStorage<T, D>(T value) { return new ReferenceStorage<T, D>(value); }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SQlizeXlsx.Linq2Excel.Validation;
using SQlizeXlsx.Linq2Excel;
using LinqToExcel;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Extends <see cref="ExcelQueryFactory"/> mapping functionality.
    /// </summary>
    public static class ColumnMapping
    {
        /// <summary>
        /// Adds property names to columns mappings.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="excelQueryFactory"></param>
        /// <param name="map"></param>
        public static void AddMappings<T>(this ExcelQueryFactory excelQueryFactory, Dictionary<string, string> map) where T : IValidatable
        {            
            map.ToList().ForEach(t =>
            {
                excelQueryFactory.AddMapping(t.Key, t.Value);                
            });
        }

        /// <summary>
        /// Creates mappings of all property names of given type to excel columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="excelQueryFactory"></param>
        public static void AddMappings<T>(this ExcelQueryFactory excelQueryFactory) where T : IMappable, new()
        {            
            T t = new T();
            var m = (IMappable)t;            
            var propertyNames = m.PropertyNames();
            var columns = m.ColumnNames();
            var i = 0;
            propertyNames.ToList().ForEach(pName =>
            {                
                excelQueryFactory.AddMapping(pName, columns.ElementAt(i));
                i++;
            });


    }
    }
}

﻿using LinqToExcel.Domain;
using SQlizeXlsx.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static SQlizeXlsx.Core.Attributes;
using static SQlizeXlsx.Core.Exceptions.AppExceptions;
using static SQlizeXlsx.Linq2Excel.Validation;
using SQlizeXlsx.Core.Randomized;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Provides methods to read excel data using LINQ and map objects to generate T-SQL statements.
    /// </summary>
    public static class SQLize
    {
        /// <summary>
        /// Reads and creates sql script per given excel data file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Task<string> ScriptAsync<T>(string filename) where T : IMappable, IValidatable, ISqlScript, new()
        {            
            return Task<string>.Run(() =>
            {
                //get linq-to-excel data
                T t = new T();
                var iMappable = (IMappable)t;
                var data = ReadExcelData<T>(@filename).ToList();
                var results = new List<Result>();
                var i = 1; // start at 1 because of excel header row!
                data.ForEach(d =>
                {
                    var isValid = d.IsValid(i);
                    if (!isValid)
                    {
                        var result = d.Validate(i);
                        results.Add(result);
                    }
                    // up the index counter
                    i++;
                });

                // ERRORS FOUND
                if (results.Count > 0)
                {
                    var message = "[" + iMappable.Name() + "]\n";
                    results.ForEach(m =>
                    {
                        message += m.Message + "\n";
                    });
                    return message;
                }
                return JoinSqlInserts(data);
            });
        }

        /// <summary>
        /// Reads and creates sql script per given excel data file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Task<ReferenceStorage<string,T>> ScriptWithResultAsync<T>(string filename) where T : IMappable, IValidatable, ISqlScript, new()
        {            
            return Task<ReferenceStorage<string,T>>.Run(() =>
            {                
                T t = new T();
                ReferenceStorage<string,T> referenceKeeper = new ReferenceStorage<string,T>();
                var iMappable = (IMappable)t;
                var data = ReadExcelData<T>(@filename).ToList();
                var random = Randomizer.Randomize<T>(data);
                var results = new List<Result>();
                var i = 1; // start at 1 because of excel header row!
                random.ToList().ForEach(d =>
                {
                    var isValid = d.IsValid(i);
                    if (!isValid)
                    {
                        var result = d.Validate(i);
                        results.Add(result);
                    }                    
                    i++;
                });

                // ERRORS FOUND
                if (results.Count > 0)
                {
                    var message = "[" + iMappable.Name() + "]\n";
                    results.ForEach(m =>
                    {
                        message += m.Message + "\n";
                    });
                    
                    referenceKeeper.Value = message;
                    referenceKeeper.IsValid = false;
                    return referenceKeeper;
                }
                // Store actual list data and SQL insert string to ref instance
                referenceKeeper.Value = JoinSqlInserts(random);
                referenceKeeper.IsValid = true;                
                referenceKeeper.Data = random;
                return referenceKeeper;
            });
        }

        /// <summary>
        /// Joins all of T instance property names of given enumeration into T-SQL insert statements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string JoinSqlInserts<T>(IEnumerable<T> data) where T : ISqlScript, new()
        {
            string script = string.Empty;
            var list = data.ToList();
            T t = new T();
            var iSqlScript = (ISqlScript)t;
            var prefix = iSqlScript.InsertPrefix();
            var quote = "'";
            list.ToList().ForEach(d =>
            {
                script += prefix;
                var properties = d.GetType().GetProperties();
                var count = properties.Count();
                var i = 1;
                foreach (var p in properties)
                {
                    var value = p.GetValue(d);                    
                    var f = p.GetCustomAttributes(typeof(RandomValue), true);                    
                    var isGeoLocation = false;
                    f.ToList().ForEach(attr =>
                    {
                        // Exclude some double values from quotations
                        RandomValue rvt = attr as RandomValue;
                        if (rvt.ElementAttributeType == ElementAttributeType.Longitude || rvt.ElementAttributeType == ElementAttributeType.Latitude)
                        {
                            isGeoLocation = true;
                        }
                    });

                    UInt32 intValue = 0;                    
                    if (value == null) // null field
                    {
                        script += "NULL";
                    }
                    else if (UInt32.TryParse((string)value, out intValue)) // numeric field
                    {
                        script += intValue;
                    }
                    else if (isGeoLocation) // double geolocation field
                    {
                        var floatValue = Single.Parse((string)value, NumberStyles.Float, CultureInfo.InvariantCulture);
                        script += floatValue.ToString(CultureInfo.InvariantCulture);
                    }
                    else // other string field
                    {
                        script += quote + p.GetValue(d) + quote;
                    }
                                        
                    if (i < count)
                    {
                        script += ","; // next field to come
                    }
                    else if (i == count) // end of complete insert statements
                    {
                        script += ");\n";
                    }
                    i++;
                }
            });
            return script;
        }

        /// <summary>
        /// Uses <see cref="LinqToExcel"/> to read Excel file located in given path.
        /// </summary>
        /// <param name="fullFilePath"></param>
        public static IEnumerable<T> ReadExcelData<T>(string fullFilePath) where T : IMappable, new()
        {
            try
            {
                var sheets = GetWorkSheets(fullFilePath).ToList();
                if (sheets.Count > 0)
                {
                    var sheet = sheets[0];
                    //var excelFile = new global::LinqToExcel.ExcelQueryFactory(fullFilePath);
                    var excelFile = new LinqToExcel.ExcelQueryFactory(fullFilePath);
                    
                    // add mappings
                    excelFile.AddMappings<T>();
                    excelFile.StrictMapping = LinqToExcel.Query.StrictMappingType.ClassStrict;
                    var data = from a in excelFile.Worksheet<T>(sheet).ToList() select a;
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                if (ex is StrictMappingException)
                {
                    throw new InvalidFormatException("Excel file is not in correct format or different data file found");
                }
                throw new InvalidFormatException(ex.Message.ToString());
            }
            //string format = f.Name + " : " + a.GetType().GetProperty(f.Name).GetValue(a, null);
        }

        /// <summary>
        /// Gets an enumerated collection of work sheets in excel file located in given path.
        /// </summary>
        /// <param name="fullFilePath"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetWorkSheets(string fullFilePath)
        {
            var excelFile = new global::LinqToExcel.ExcelQueryFactory(fullFilePath);
            var worksheetsList = excelFile.GetWorksheetNames();
            return excelFile.GetWorksheetNames();
        }

        /// <summary>
        /// Validates columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sheetName"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static IEnumerable<string> ValidateColumns<T>(string sheetName, string fullPath) where T : new()
        {
            // Read Excel spredsheet        
            var excelFile = new global::LinqToExcel.ExcelQueryFactory(fullPath);

            // Grab the column names from worksheet
            var sheetColumnNames = excelFile.GetColumnNames(sheetName);

            // Grab a list of the properties exposed by class instance:            
            T t = new T();
            var ClassProperties = t.GetType().GetProperties();

            // Create an enumerable containing the property names:
            var requiredColumns = (from p in ClassProperties select p.Name);

            // Copy the names into missing columns list:
            var missingColumns = requiredColumns.ToList();

            foreach (string column in requiredColumns)
            {
                foreach (var candidate in sheetColumnNames)
                {
                    if (column.Equals(candidate, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Any items left represent properties which were
                        // not found to match in the column names (case-insensitive):
                        missingColumns.Remove(column);
                    }
                }
            }
            return missingColumns;
        }
    }
}

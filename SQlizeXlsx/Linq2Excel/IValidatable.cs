﻿using static SQlizeXlsx.Linq2Excel.Validation;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Provides functionality to validate an object mapped to excel row against custom rules.
    /// </summary>
    public interface IValidatable
    {
        // Returns the validation result of mapped object.
        Result Validate(int row);
    }
}

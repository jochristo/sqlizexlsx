﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQlizeXlsx.Linq2Excel
{

    /// <summary>
    /// Provides functionality to retrieve mappings, columns, and property names mapped to excel column names.
    /// </summary>
    public interface IMappable
    {
        // Gets type's property names.
        IEnumerable<string> PropertyNames();
        
        // Gets column names of mapped excel file.
        IEnumerable<string> ColumnNames();
        
        // Gets a mappings dictionary of property names to column names.
        Dictionary<string, string> Mappings();

        // Returns a name for mapped POCO
        string Name();
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static SQlizeXlsx.Core.Exceptions.AppExceptions;

namespace SQlizeXlsx.Linq2Excel
{
    /// <summary>
    /// Provides validation functionality and extention methods to <see cref="IValidatable"/> types.
    /// </summary>
    public static class Validation
    {
        /// <summary>
        /// Represents validation result with success indication and message.
        /// </summary>
        public class Result
        {
            public Result(bool success, string message)
            {
                Success = success;
                var trimmed = message.Trim();
                Message = Regex.Replace(trimmed, @"\s{2,}", " ");
            }
            public bool Success { get; }
            public string Message { get; }
        }
        
        #region Special Field Validation

        public static bool isPhraseValid(string value, int gaps)
        {
            const string PHRASE_PATTERN = @"(^\s*[^\[\]\s]+(\s+[^\[\]\s]+)*(\s+\[\])+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)|(^\s*(\[\])(\s+\[\])*(\s+[^\[\]\s]+)+((\s+\[\])*(\s+[^\[\]\s]*)*)*$)";
            Regex regex = new Regex(PHRASE_PATTERN);
            return regex.IsMatch(value);
        }

        public static bool isGapsValid(string phrase, int gaps, out int found)
        {
            List<int> positions = new List<int>();
            int pos = 0;
            var pattern = "[]";
            while ((pos < phrase.Length) && (pos = phrase.IndexOf(pattern, pos)) != -1)
            {
                positions.Add(pos);
                pos += pattern.Length;
            }
            found = positions.Count;
            if (found == gaps)
            {
                return true;
            }
            return false;
        }

        public static bool isAnswerValid(string answer, int gaps)
        {
            try
            {
                AnswerJson[] answerJson = JsonConvert.DeserializeObject<AnswerJson[]>(answer);
                return answerJson != null;
            }
            catch (Exception ex)
            {
                throw new InvalidJsonFormatException(ex.Message.ToString());
            }
        }

        public static bool isJsonAnswer(string answer, out string result)
        {
            try
            {
                AnswerJson[] answerJson = JsonConvert.DeserializeObject<AnswerJson[]>(answer);
                result = "isJson";
                return answerJson.Count() > 0;
            }
            catch (Exception ex)
            {
                result = ex.Message.ToString();
                return false;
            }
        }

        public static bool isJsonAnswer(string answer, out JsonValidationResult jsonValidationResult, out string message)
        {
            try
            {
                AnswerJson[] answerJson = JsonConvert.DeserializeObject<AnswerJson[]>(answer);
                jsonValidationResult = JsonValidationResult.IsJson;
                if (answerJson.Count() == 0)
                {
                    jsonValidationResult = JsonValidationResult.EmptyJson;
                }
                message = null;
                return true;

            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
                jsonValidationResult = JsonValidationResult.NotJson;
                return false;
            }
        }

        public static bool isJsonAnswerValid(string answer)
        {
            string patternInclAllButSquareBrackets = @"^\s*\[\s*({\s*""[w][o][r][d]""\s*:\s*""\s*[^\s\[\]""]+(\s*[^\s\[\]""]+\s*)*""\s*}\s*){1}(,\s*{\s*""[w][o][r][d]""\s*:\s*""\s*[^\s\[\]""]+(\s*[^\s\[\]""]+\s*)*""\s*}\s*)*\]\s*$";
            Regex regex = new Regex(patternInclAllButSquareBrackets);
            return regex.IsMatch(answer);
        }

        #endregion

    }


}
